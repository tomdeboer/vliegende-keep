#ifndef __AVR_ATmega328P__
#define __AVR_ATmega328P__
#endif

#include <Servo.h>
#include <util/delay.h>
#include <avr/io.h>
#include <arduino.h>

Servo servo;
int rotate = 0;

int main(){
    init();

    servo.attach(9);
    
    DDRD &= ~_BV(PORTD6) | ~_BV(PORTD7);
    PORTD &= ~_BV(PORTD6) | ~_BV(PORTD7);

    for(;;){

        if(digitalRead(7)) rotate -= 60;
        if(digitalRead(6)) rotate += 60;
        
        rotate = rotate % 181;
        
        servo.write(rotate);
        _delay_ms(300);

    }

return 0;
}