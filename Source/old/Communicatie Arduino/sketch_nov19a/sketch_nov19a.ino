#include <Arduino.h>

typedef enum {NONE, xCoordinate, yCoordinate} states;

states state = NONE;

uint16_t incomingByte, xCoord, yCoord;

/* function to print and record the coordinates*/

void printCoord(){
  switch (state){
  case xCoordinate:
    xCoord = incomingByte;
    Serial.print ("xCoordinate = ");
    Serial.println (xCoord);
    break;
  case yCoordinate:
    yCoord = incomingByte;
    Serial.print ("yCoordinate = ");
    Serial.println (yCoord);
  case NONE:
    break;
  }
  incomingByte = 0;
}

/*
 * Since we can only receive one byte at the time, we need to create a buffer
 * where we can combine these bytes to form a whole number. To see when a number
 * starts we add a  "x" or "y" depending on what kind of coordinate we are sending.
 * We then multiply it with ten and add the next number to it and so on.
 * after that we subtract the char 0 (zero) to make the it a number instead of a character
 */

void combineByte (const byte coordinate){
	Serial.println("CombineByte");
    // check if the incoming byte is a digit or a char
  if (isdigit (coordinate)){
//    digitalWrite(13,LOW);
    incomingByte *= 10;
    incomingByte += coordinate - '0';
  }
  else{
    printCoord ();
    switch (coordinate){
    case 'x':
      state = xCoordinate;
      break;
    case 'y':
      state = yCoordinate;
      break;
    default:                            // to close the command, enter a random character like "e"
      state = NONE;
      break;
    }
  }
}

int main (void){
  init();
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  Serial.begin (115200);
  for(;;){
    if (Serial.available ()){combineByte (Serial.read ());} // when a byte is received it will be combined to form a whole number
  }
}

