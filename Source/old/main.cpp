#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
// #include <opencv2/core/utility.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/nonfree/nonfree.hpp>
#include <sstream>
#include <stdio.h>
#include <string>
#include <time.h>
#include <vector>
#include <Windows.h>

#define FRAME_COUNT 100
#define FLAG_CAPTURE 0x1
#define FLAG_SAVE 0x2
#define FLAG_LOAD 0x4
using namespace std;
using namespace cv;

int threshold_value = 0;
int threshold_type = 3;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;
Mat frame;
Mat storeResult[FRAME_COUNT];
Point storeCentroid[FRAME_COUNT];

template <typename T>
string numberToString(T pNumber) {
 ostringstream oOStrStream;
 oOStrStream << pNumber;
 return oOStrStream.str();
}


cv::Point getCentroid(cv::Mat img);
void saveFilm();
void loadFilm();
int captureCamData();

vector<Mat> frames;
string window_name = "PS3 eye - Camera";
string trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
string trackbar_value = "Value";
string fileName = "temp.xml";
byte parameters_flag = 0x00;

void Threshold_Demo( int, void* ); 
/**
 * @function main
 */
int main(int argc, char** argv ) {
	int err;

	for (int i = 1; i < argc; i++) {
		if (string(argv[i]).compare("capture") == 0)
			parameters_flag |= FLAG_CAPTURE; 
		if (string(argv[i]).compare("save") == 0) 
			parameters_flag |= FLAG_SAVE;
		if (string(argv[i]).compare("load") == 0) 
			parameters_flag |= FLAG_LOAD;
	}

	
	//--------------- CAPTURE CAM DATA
	if (parameters_flag & FLAG_CAPTURE) {
		err = captureCamData();
		if (err) return -1;
	}

	//--------------- SAVE FILM
	if (parameters_flag & FLAG_SAVE) {  
	    saveFilm();    
	}

    //--------------- LOAD FILM
	if (parameters_flag & FLAG_LOAD) {
	    loadFilm();
	}

    for(int i = 0; i < FRAME_COUNT; i++) {
         //line(window_name, scene_corners[0] + Point2f( card.cols, 0), scene_corners[1] + Point2f( card.cols, 0), Scalar(0, 255, 0), 4 );

        Point p = Point(storeCentroid[i]);
        Point q = Point(p);

        p.x -= 5;
        p.y -= 5;
        q.x += 5;
        q.y += 5;

        if (p.x > 0 and p.y > 0) {
            rectangle(storeResult[i], p, q, Scalar(0, 0, 255), 1, 8, 0);
        }

        imshow(window_name, storeResult[++i]);
        // if(waitKey(30) >= 0) break;
        if(waitKey(300) >= 0) break;
    }
    return 0;
}

cv::Point getCentroid(cv::Mat img) {
    cv::Point Coord;
    cv::Moments mm = cv::moments(img,false);
    double moment10 = mm.m10;
    double moment01 = mm.m01;
    double moment00 = mm.m00;
    Coord.x = int(moment10 / moment00);
    Coord.y = int(moment01 / moment00);
    return Coord;
}

//save all the meta-deta from the cards that are stored as pictures in the folder "cards"
void saveFilm() {
    FileStorage fs(fileName, FileStorage::WRITE);                                    //use an openCV function to store the data in an 
                                                                                     //XML file
    fs << "frameCount" << FRAME_COUNT;                                             	 //store how many cards are going to be stored
    time_t rawtime; time(&rawtime);
    fs << "calibrationDate" << asctime(localtime(&rawtime));                         //and at what time it was stored
    for (int i = 0; i < FRAME_COUNT; i++) {                                          //write all data belonging to a card
        //-- Step 3: Store the results in the XML File                               //Store the results to an XML file
        fs << string("frame") + numberToString(i) << "{";
        fs << "Mat" << storeResult[i];
        fs << "centroid" << "{";
            fs << "x" << storeCentroid[i].x;
            fs << "y" << storeCentroid[i].y;
        fs <<   "}";
        fs << "}"; 
    }
    fs.release();                                                                    //close file
}

void loadFilm() {
    FileStorage fs(fileName, FileStorage::READ);
    FileNode fn;
    int count = fs["frameCount"];
    //cout << "I found " << count << " frames in this video" << endl;
    for (int i = 0; i < count;++i) {
        string node = string("frame") + numberToString(i);
        // cout << (string)(fs[node]["name"]) << endl;

        fn = fs[node]["Mat"];
        Mat& tempFrame = storeResult[i]; 
        read(fn, tempFrame);

        storeCentroid[i].x = fs[node]["centroid"]["x"];
        storeCentroid[i].y = fs[node]["centroid"]["y"];

       cout << i + 1 << "    " << storeCentroid[i].x << "  " << storeCentroid[i].y << endl;
    }
    fs.release();
}

int captureCamData() {
    // VideoCapture cap(2); // open the default camera
	VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return 1;
    cap.set(CV_CAP_PROP_FPS, 180);
    cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);
    //cv::SetCaptureProperty(cap, cv.CV_CAP_PROP_FPS,30);
    //namedWindow(window_name,1);
      time_t rawtime; 
      time(&rawtime);
      cout << asctime(localtime(&rawtime));
    for(int i = 0; i < FRAME_COUNT; i++) { 
    //for(;;) { 
        cap >> frame; // get a new frame from camera

        cvtColor(frame, frame, COLOR_BGR2HSV);
        cv::inRange(frame,cv::Scalar(5, 110, 110),cv::Scalar(15,255,255),frame);
        Point p = getCentroid(frame);
        storeCentroid[i] = Point(p);
#ifdef DEBUG
        Point q = Point(p);

        p.x -= 5;
        p.y -= 5;
        q.x += 5;
        q.y += 5;

      //  printf("[%d, %d]", p.x + 5, p.y + 5);
        if (p.x > 0 and p.y > 0) {
        //     //void rectangle(Mat& img, Point pt1, Point pt2, const Scalar& color, int thickness=1, int lineType=8, int shift=0)
            rectangle(frame, p, q, Scalar(0, 0, 255), 1, 8, 0);
        }
#endif        
        //imshow(window_name, frame);
        storeResult[i] = frame;
        //cout << i << ". " << "rows: " << frame.rows << endl;
        if(waitKey(0.1) >= 0) break;
    }
    time(&rawtime);
    cout << asctime(localtime(&rawtime));
    return 0;
}