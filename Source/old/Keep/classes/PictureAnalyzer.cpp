/* 
 * File:   PictureAnalyzer.cpp
 * Author: Paul van Vulpen
 * 
 * Created on November 12, 2013, 2:38 PM

 SHELL COMMANDO: 
 convert a.bmp -compress none -color 8 BMP2:t1.bmp
 74 bytes info

 */

#include "../headers/PictureAnalyzer.h"
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <iostream>
#include <iomanip> //print hex to stream
#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>
using namespace cv;

#define IMG_FILE_HEADER_SIZE    74			//
#define COLOR_ORANGE_HEX        0x22
#define PICTURE_WIDTH           320
#define PICTURE_HEIGHT          240
#define COLOR_COUNT             7
#define ORANGE_RGBSCALE         380

/**
 * constructors
 */
PictureAnalyzer::PictureAnalyzer() {
	this->picture = picture;
}

/**
 * determine balLX and ballY based on the FILE named [fileName]
 * current algorithm has a speed of less than 1 ms per frame on average as per 26 november 2013
 * PROCESSOR: i5-2410M @ 2.3Ghz, RAM: 8GB,  
 */
BallPos PictureAnalyzer::scanPicture(char fileName[45]) {
    char pad[45] = "./images/";
//    char fileName[45] = "i4.bmp";
    char afbeelding[90];
    strcpy(afbeelding, strcat(pad, fileName));
    std::ifstream input(afbeelding, std::ios::in | std::ios::binary ); //binary input stream naar testafbeelding
    int8_t m=-1; //buffer
    uint8_t r, g, b, //red green blue 
            orangeValue; //which value in the pallet represents orange (the ball)
    int16_t rgbScaledValue = -1; //a scaled calculation to decide which color is closest to orange
    uint32_t i=0; //counter;
    // input.seekg(18); //skip first 18 bytes, next value contains width 
    input.seekg(26); //color pallet
    for (uint8_t counter = 0; counter < COLOR_COUNT; counter++) { //scan entire color pallet
        input.read(reinterpret_cast <char * > (&b), sizeof(b));  //collect blue-value data
        input.read(reinterpret_cast <char * > (&g), sizeof(g));  //green-value
        input.read(reinterpret_cast <char * > (&r), sizeof(r));  //red-value
//        std::cout << '#' << std::dec << (int)r << ',' << (int)g << ',' << (int)b << std::endl;
        if (rgbScaledValue < 0 || (b < 110 && abs(r+g - b/2 - ORANGE_RGBSCALE) < abs(rgbScaledValue - ORANGE_RGBSCALE))  ) { //not decided or value is closer to orange
            rgbScaledValue = r+g - b/2; //found value closer to orange
//            std::cout << "value" << std::dec << (int)counter << ": " << rgbScaledValue << std::endl;
            orangeValue = 0x11 * counter; //search 2 pixels of this color
        }
//        else 
//           std::cout << "not closer" << std::dec << (int)counter << ": " << abs(r+g - b/2 - ORANGE_RGBSCALE) << std::endl;  
    }    
//    std::cout << "search for " << std::hex << (int)orangeValue << std::endl;
    
    input.seekg(IMG_FILE_HEADER_SIZE);
	while (m != orangeValue && input.read(reinterpret_cast < char * > (&m), sizeof(m))) { //interpreteer de char* alsof het uint8_t informatie is.
        ++i; 
//        std::cout << std::hex << (int)m << " ";
//        printf("0x%.2X\n", (int)m);
//        printf("%d\n", m);
    }
    --i; //pointer is automatically increased after reading from the ifstream; reset to wanted position
    i *= 2; //2 pixels per byte;
#ifdef DEBUG_MODE
    printf("%s, position: [%d, %d]\n", fileName, i - ((i/PICTURE_WIDTH) * PICTURE_WIDTH), i/PICTURE_WIDTH);			//print resultaat hexadecimaal
#endif    
    input.close();
    BallPos bal;
    bal.y = i/PICTURE_WIDTH;
    bal.x = i - bal.y * PICTURE_WIDTH; 
    return bal;
}; // determine ballX, ballY;

void PictureAnalyzer::test(){
    Mat img;
//    VideoCapture cap(0);
    /*while (cap.isOpened()) {
        cap >> img;
        if ( !img.empty() ) {
            
            //processing code here
            imshow("image", img);
        }
        char k = (char) waitKey(10); //sleep for 10ms
        if (k == 27) break; // 'esc' pressed
    }*/
}