/* 
 * File:   PictureAnalyzer.h
 * Author: Paul van Vulpen
 *
 * Created on November 12, 2013, 2:38 PM
 */

#ifndef PICTUREANALYZER_H
#define	PICTUREANALYZER_H
//#define DEBUG_MODE
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <iostream>
#include <iomanip> //print hex to stream
#include <fstream>

typedef struct {
	uint16_t x, y;
} BallPos;

class PictureAnalyzer {
private:
    uint16_t ballX, ballY;
    FILE *   picture;
    std::string path; 
public:
    PictureAnalyzer();    
    void test();
    BallPos scanPicture(char fileName[45]); // determine ballX, ballY;

};

#endif	/* PICTUREANALYZER_H */

