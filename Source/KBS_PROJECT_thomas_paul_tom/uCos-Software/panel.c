/*
 * panel.c
 *
 *  Created on: 20 mei 2014
 *      Author: Paul van Vulpen
 */
#include "stdafx.h"
#include "panel.h"
#include "LCD_content.h"
#include "enum_panel.h"
#include "globals.h"



void LCD_prepareContent(Panel currentPanel, LCD_content* dataIn) {
  //center the headline
  centerString(dataIn -> line1);

  //create the second line based on the values.

  switch (currentPanel) {
  	  case NONE: break;
  	  case SCORE:
  	    sprintf((dataIn -> line2), "%3d - %3d",
  	                dataIn -> value.teamData.valueRed,
  	                dataIn -> value.teamData.valueBlue);
  	    centerString(dataIn -> line2);
        addRedAndBlueTag(dataIn -> line2);
  	    break;

  	  case POSSESSION:
  	    sprintf(dataIn -> line2, "%2d%%- %2d%%",
  	                dataIn -> value.teamData.valueRed,
  	                dataIn -> value.teamData.valueBlue);
  	    centerString(dataIn -> line2);
  	    addRedAndBlueTag(dataIn -> line2);
  	    break;

  	  default:
  		  sprintf(dataIn -> line2, "%d", dataIn -> value.valueOther);
  		  centerString(dataIn -> line2);
  		  break;
  }

}

void centerString(char * str) {
    char line1[LINE_LENGTH + 1]; //line length + \0
    INT8U pos, i;

    //prepare center position
    pos = (LINE_LENGTH - strlen(str)) / 2;

    //backup original line
    strcpy(line1, str);

    //clear original line
    memset(str, ' ', LINE_LENGTH);

    //center Line 1:
    for (i = 0; pos < LINE_LENGTH && line1[i] != '\0'; ++pos, ++i)
      *((str) + pos) = line1[i];
}

void emptyString(char * str) {
    memset(str, ' ', LINE_LENGTH);
}

//add "R>  <B" to the second line to point to what team the score belongs to
void addRedAndBlueTag(char * str) {
    str[0]  = 'R';
    str[1]  = '>';
    str[LINE_LENGTH-2] = '<';
    str[LINE_LENGTH-1] = 'B';
}

bool dataChanged(Panel lastPanel, LCD_content* data) {
  if (lastPanel == currentPanel) {
    switch (currentPanel) {
    	case NONE:
    		return false;
        case SCORE:
          if (data -> value.teamData.valueRed == score[RED] &&
              data -> value.teamData.valueBlue == score[BLUE])
                return false;
          break;

        case POSSESSION:
          if (data -> value.teamData.valueRed == possession[RED] &&
              data -> value.teamData.valueBlue == possession[BLUE])
                return false;
          break;

        default:
          if (data -> value.valueOther == *currentPanelValue)
        	  return false;
          break;
    }
  }

  return true;
}

void updateLCDMessage(LCD_content* dataIn) {
	INT8U i;
	for (i = 0; i < LINE_LENGTH; ++i) {
		lcdMessage[i]		 	  = (dataIn -> line1)[i];
		lcdMessage[i+LINE_LENGTH] = (dataIn -> line2)[i];
	}
	lcdMessage[32] = '\0';
	printf("%s\n", lcdMessage);
}
