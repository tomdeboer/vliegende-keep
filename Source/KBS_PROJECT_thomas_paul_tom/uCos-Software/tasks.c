/*
 * tasks.c
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen
 */

#include "stdafx.h"
#include "panel.h"
#include "LCD_content.h"
#include "enum_panel.h"
#include "tasks.h"
#include "globals.h"

OS_STK    tStartStack[TASK_STACKSIZE];    // stack van TaskStart
OS_STK    tUpdateCurrentPanelStack[TASK_STACKSIZE];

void TaskStart(void* pdata) {
  //--------------------- D O E S N ' T   E X I S T  Y E T ----------
  //LCD_clear();
  //--------------------- D O E S N ' T   E X I S T  Y E T ----------

    OSTaskCreate(TaskUpdateCurrentPanel, (void *) 0,
      &tUpdateCurrentPanelStack[TASK_STACKSIZE - 1], UPDATE_PANEL_PRIORITY);
    printf("activated\n");
    while (1) {
       OSTimeDlyHMSM(1, 0, 0, 0);
    } //infinite loop
}

void TaskUpdateCurrentPanel(void* pdata) {
  LCD_content dataOut;
  Panel lastPanel = NONE;

  while (1) {
    emptyString(dataOut.line1);
    emptyString(dataOut.line2);

    //check for a panel- or data change.
    if (dataChanged(lastPanel, &dataOut)) {
      //link the new data to the struct
      switch (currentPanel) {
        case NONE: break;
        case SCORE:
            strcpy(dataOut.line1, "SCORE:");
            dataOut.value.teamData.valueRed = score[RED];
            dataOut.value.teamData.valueBlue = score[BLUE];
            currentPanelValue = NULL;
            break;

        case POSSESSION:
            strcpy(dataOut.line1, "POSSESSION:");
            dataOut.value.teamData.valueRed = possession[RED];
            dataOut.value.teamData.valueBlue = possession[BLUE];
            currentPanelValue = NULL;
            break;

        case TOTAL_DISTANCE:
            strcpy(dataOut.line1, "TOTAL DISTANCE:");
            dataOut.value.valueOther = totalDistance;
            currentPanelValue = &totalDistance;
            break;
        case AVG_SPEED:
            strcpy(dataOut.line1, "AVERAGE SPEED:");
            dataOut.value.valueOther = avgSpeed;
            currentPanelValue = &avgSpeed;

            break;
        case MAX_SPEED:
        	strcpy(dataOut.line1, "MAX SPEED:");
        	dataOut.value.valueOther = maxSpeed;
        	currentPanelValue = &maxSpeed;
        	break;

        case CURRENT_SPEED:
        	strcpy(dataOut.line1, "CURRENT SPEED:");
        	dataOut.value.valueOther = currentSpeed;
        	currentPanelValue = &currentSpeed;
        	break;
      }

       LCD_prepareContent(currentPanel, &dataOut);

       printf("%s\n%s\n\n", dataOut.line1, dataOut.line2);
       updateLCDMessage(&dataOut);
       lastPanel = currentPanel;
    }

    OSTimeDlyHMSM(0, 0, 1, 0);
  }
}
