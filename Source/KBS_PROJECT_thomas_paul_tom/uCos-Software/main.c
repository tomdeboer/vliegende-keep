
#include "stdafx.h"
#include "panel.h"
#include "enum_panel.h"
#include "tasks.h"
#include "globals.h"

OS_STK    tStartStack[TASK_STACKSIZE];    // stack van TaskStart
OS_STK    tUpdateCurrentPanelStack[TASK_STACKSIZE];

int main(void) {
  OSInit();
  currentPanel      = POSSESSION;
  totalDistance     = 1024;
  avgSpeed          = 40;
  score[BLUE]       = score[RED]      = 2;
  possession[BLUE]  = possession[RED] = 50;
  
  OSTaskCreate(TaskStart, (void *) 0,
    &tStartStack[TASK_STACKSIZE - 1], START_PRIORITY);  

  OSStart();
  return 0; //should never be reached
}

