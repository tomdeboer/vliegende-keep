#ifndef ENUM_PANEL_
#define ENUM_PANEL_

typedef enum {
    NONE,
    SCORE,
    POSSESSION,
    TOTAL_DISTANCE,
    AVG_SPEED,
    MAX_SPEED,
    CURRENT_SPEED
} Panel;

#endif
