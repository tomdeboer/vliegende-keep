/*
 * globals.h
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_
#include "stdafx.h"
#include "panel.h"

//defines
#define bool            BOOLEAN
#define false           0
#define true            1
#define TASK_STACKSIZE  512
#define BLUE            0
#define RED             1
#define LINE_LENGTH		16

//Definition of Task Priorities
#define START_PRIORITY            6
#define UPDATE_PANEL_PRIORITY     7
#define TASK2_PRIORITY            8

Panel     currentPanel;
INT8U     score[2];
INT8U     possession[2];
INT16U    totalDistance, avgSpeed, maxSpeed, currentSpeed;
INT16U    *currentPanelValue;
char	  lcdMessage[33];

#endif /* GLOBALS_H_ */
