
#define _CRT_SECURE_NO_WARNINGS
/* actual field width = 615 mm , actual white border with = 40mm 
	since only the field is detected, we need to calculate how many pixels we need to add to include the white border
*/

#include "stdafx.h"
#include "ballDetection.h"
#include "cvExtension.h"

using namespace std;				//standard C++
using namespace cv;					//openCV
using namespace cvExtension;			//our personal openCV functions

template <typename T>				//to easily convert an string containing a number to a string
string numberToString(T pNumber) {
	ostringstream oOStrStream;
	oOStrStream << pNumber;
	return oOStrStream.str();
}

extern atomic<bool> requestExitProgram;

//----------------------- C O N S T R U C T O R ----------------------\\ 
BallDetection::BallDetection() {
	cap.open(TOPVIEW_CAMERA);
	fileName = "temp.xml";
	window_name = "PS3 eye - Camera";
	parametersFlag  = 0x00; //flaggroup to decide which actions are to be performed by the application
	ret 		    = 0;
	lastFrame		= NULL;
	usbConnectionHandle = UsbConnection(MY_VID, MY_PID);
}
//--------------------------------------------------------------------//


//-------------------------- F U N C T I O N S -----------------------\\


//find the center of the ball.
//	img: picture to evaluate
cv::Point BallDetection::getCentroid(cv::Mat img) {
	//--- INIT ---
	vector<vector<Point>> contours; // Vector for storing contour
	vector<Vec4i> hierarchy;		// Contour in contour
	Point Coord;					// return value
	Mat edges;						// storage for edge detection result
	int largestArea = 0;			// store the largest white 'blob' oof the img
	
	//--- BODY ---
	Canny(img, edges, 0, 255, 3, false); //perform edge detection on "img"
	findContours(edges, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE); // Find the contours in the image

	for (int i = 0; i < contours.size(); ++i) { 		 // iterate through each contour.
		double a = contourArea(contours[i], false);  	 // Find the area of contour
		Rect temp = boundingRect(contours[i]);			 // get the boundaries of the detected contour
		
		if (a > largestArea && temp.width <= BALL_PIX_WIDTH_MAX &&  //if this is the largest detected object that fits the maximum ball size.
			temp.height <= BALL_PIX_HEIGHT_MAX) {					//store the result
			largestArea = (double)a;
			//printf("ball W: %d - H: %d\n", temp.width, temp.height);
			ballRect = boundingRect(contours[i]);  // Find the bounding rectangle for biggest contour
			break;
		}
	}

	rectangle(edges, ballRect,  Scalar(36,153,255), 1, 8, 0); //draw a rectangle on the img to be displayed
	//imshow("edges", edges); 									  //display the result of the edge detection
	Coord.x = ballRect.x + (ballRect.width / 2);	//find the center based on the width of the bounding box of the ball
	Coord.y = ballRect.y + (ballRect.height / 2);	//find the center based on the height of the bounding box of the ball
	return Coord;
}

//save each captured frame in an XML-file
void BallDetection::saveFilm() {
	cout << "save Film" << endl;
	FileStorage fs(fileName, FileStorage::WRITE);               //use an openCV function to store the data in an 
																//XML file
	fs << "frameCount" << FRAME_COUNT;                          //store how many frames are going to be stored
	time_t rawtime; time(&rawtime);
	fs << "calibrationDate" << asctime(localtime(&rawtime));    //and at what time it was stored
	for (int i = 0; i < FRAME_COUNT; i++) {                     //write all data belonging to a card
		//-- Step 3: Store the results in the XML File          //Store the results to an XML file
		fs << string("frame") + numberToString(i) << "{";
		fs << "Mat" << storeResult[i];
		fs << "centroid" << "{";
			fs << "x" << storeCentroid[i].x;
			fs << "y" << storeCentroid[i].y;
		fs <<   "}";
		fs << "}"; 
	}
	fs.release(); 												//close file
}

//load an XML-file and store each frame in a vector
void BallDetection::loadFilm() {
	cout << "load Film" << endl;							
	FileStorage fs(fileName, FileStorage::READ); 				//use an openCV function to load the data from an
																//XML file
	FileNode fn;												//pointer to the node to be operated
	int count = fs["frameCount"];								//retrieve the framecount

	for (int i = 0; i < count;++i) {							//for each frame
		string node = string("frame") + numberToString(i);		//construct the name of the current frame
		// cout << (string)(fs[node]["name"]) << endl;

		fn = fs[node]["Mat"];									//connect to the node
		Mat& tempFrame = storeResult[i]; 						//create a reference to the Mat in which we like to store the content of the Node
		read(fn, tempFrame);									//node -> vector

		storeCentroid[i].x = fs[node]["centroid"]["x"];			//retrieve the x-position of the ball in the given frame
		storeCentroid[i].y = fs[node]["centroid"]["y"];			//retrieve the y-position of the ball in the given frame

	   cout << i + 1 << "    " << storeCentroid[i].x << "  " << storeCentroid[i].y << endl; //print positions to console
	}
	fs.release();


	//---------- SHOW ------
	for(int i = 0; i < FRAME_COUNT; i++) {	//foreach frame
		 //line(window_name, scene_corners[0] + Point2f( card.cols, 0), scene_corners[1] + Point2f( card.cols, 0), Scalar(0, 255, 0), 4 );

		Point p = Point(storeCentroid[i]);	//retrieve the centerpoint of the ball in the given frame
		Point q = Point(p);					//create an offset to display a square.

		p.x -= 5;	//create a square around the frame
		p.y -= 5;
		q.x += 5;
		q.y += 5;

		if (p.x >= 0 && p.y >= 0) { //when the topleft point is within the frame 
			rectangle(storeResult[i], p, q, Scalar(0, 0, 255), 1, 8, 0); //display the rectnagle on the square on the screen
		}

		//imshow(window_name, storeResult[++i]); //show the result frame for frame
		// if(waitKey(30) >= 0) break;
		if(waitKey(300) >= 0) break; //@30 fps
	}
	//--------- END SHOW -----------
}


//capture framedata from the camera
int BallDetection::captureCamData() {
	cout << "capture cam " <<  this << endl;
	Mat rawFrame, filterOrangeFrame, 
		filterMovementFrame1, filterMovementFrame2,
		differenceImage, thresholdImage;		//used for the movement algorithm

	cout << "capture cam data" << endl;
	time_t rawtime; 							//display the time of capture
	time(&rawtime);								//
	cout << asctime(localtime(&rawtime));		//

	ofstream logFile;
  	logFile.open ("xy_log.csv");

	//for(int i = 0; i < FRAME_COUNT; i++) { 	//capture a specific amount of frames
	for(;;) { 								//infinite	
		cap >> rawFrame; 						//get a new frame from the camera
		//if (lastFrame.cols <= 0) {				//if this is the first frame 
		//	rawFrame(innerFieldRect).copyTo(lastFrame); //copy the just received frame to "lastframe"
		//	cap >> rawFrame;					//and retrieve a new frame
		//}
		rawFrame(innerFieldRect).copyTo(frame); //crop the image to only display the ROI (region of interest)
		
		//------   MOVEMENT DETECTION ----
		//lastFrame.copyTo(filterMovementFrame1); //create a copy of the last two frames
		//frame.copyTo(filterMovementFrame2);		//
		//movementFilter(filterMovementFrame1, filterMovementFrame2, differenceImage, thresholdImage); //apply the movement filter
		
		filterOrangeFrame = Mat(frame);						//storage for the filtered frame
		

#ifdef BRIGHT_BALL
		colorFilter(filterOrangeFrame, Scalar(5, 110, 110), Scalar(15, 255, 255));	//apply the color filter for "bright"-ball
#else
		colorFilter(filterOrangeFrame, Scalar(0, 75, 75), Scalar(255, 134, 255));	//apply the color filter for "pro-play"-ball
#endif
		medianBlur(filterOrangeFrame, filterOrangeFrame, 7); //blur the result for noise reduction
		cv::Point p = getCentroid(filterOrangeFrame);		//retrieve center point of the orange object
	    //storeCentroid[i] = cv::Point(p);
		
		//printf("%d - %d\n", p.x, p.y);
  		logFile << p.x << ';' << p.y << endl;

		//prepare the data to be sent to the ISR1362 (over USB)
		if (p.x > 255) {			
			usbConnectionHandle.setTbuf(0, p.x % 256);
			usbConnectionHandle.setTbuf(1, p.x >> 8 | X_OPCODE); //combine the most significant bits with the opcode
			//tbuf[0] = p.x % 256;			
			//tbuf[1] = p.x >> 8 | X_OPCODE; //combine the most significant bits with the opcode
		} else {
			usbConnectionHandle.setTbuf(0, p.x);
			usbConnectionHandle.setTbuf(1, X_OPCODE); //combine the most significant bits with the opcode
			//tbuf[0] = p.x;
			//tbuf[1] = X_OPCODE; //combine the most significant bits with the opcode
		}

		if (p.y > 255) {
			usbConnectionHandle.setTbuf(2, p.y % 256);
			usbConnectionHandle.setTbuf(3, p.y >> 8 | Y_OPCODE); ////combine the most significant bits with the opcode
			//tbuf[2] = p.y % 256;
			//tbuf[3] = p.y >> 8 | Y_OPCODE; //combine the most significant bits with the opcode
		} else {
			usbConnectionHandle.setTbuf(2, p.y);
			usbConnectionHandle.setTbuf(3, Y_OPCODE); //combine the most significant bits with the opcode
			//tbuf[2] = p.y;
			//tbuf[3] = Y_OPCODE; //combine the most significant bits with the opcode
		}

		usbConnectionHandle.bulkWrite(); //write the data to the ISP1362

#ifdef DEBUG
		Point q = Point(p);		//draw a small square on the screen

		// p.x -= 5;
		// p.y -= 5;
		q.x += 1; 	//second point nearby to 
		q.y += 1;	//create a square

	  //  printf("[%d, %d]", p.x + 5, p.y + 5);
		if (p.x > 0 && p.y > 0) 
			rectangle(filterOrangeFrame, p, q, Scalar(0, 0, 255), 1, 8, 0);	
	
#endif        
		//imshow(window_name, filterOrangeFrame);

		if (requestExitProgram) break;
		//if(waitKey(1000/FRAME_RATE) >= 0) break; //go through the loop at given framerate
		//rawFrame(innerFieldRect).copyTo(lastFrame); //update the currentframe to be the lastframe
	}

	cout << "done capturing" << endl;
	time(&rawtime);							//retrieve the time
	cout << asctime(localtime(&rawtime));	//print the time again
	logFile.close();
	return 0;
}


//find the size of the playfield
int BallDetection::defineFieldSize() {
	cout << "define field size" << endl;
	if (!setCamera()) {							//make sure a camera is found
		cout << "camera not found!" << endl;
		exit(2);
	}

	int largestArea;				//store the largest rectangle (this is the playfield)
	int take10Frames = 0;			//make sure the picture is stable by taking 5 frames

	vector<vector<Point>> contours; // Vector for storing contour
	vector<Vec4i> hierarchy;		// Contour in contour

	do {
		do {
			cap >> frame; 				//capture a frame from the camera
		} while (frame.cols <= 0);

		largestArea = 0;			//reset the framesize

		Mat thr(frame.rows, frame.cols, CV_8UC1);				  //create empty image
		Mat dst(frame.rows, frame.cols, CV_8UC1, Scalar::all(0)); //black image of same size

		cvtColor(frame, thr, CV_BGR2GRAY); 					   	//Convert to gray
		threshold(thr, thr, 80, 120, THRESH_BINARY); 		    //Threshold the gray to find the table on the blue floor

		imshow("threshold", thr);

		findContours(thr, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE); // Find the contours in the image

		for (int i = 0; i < contours.size(); ++i) { 		 // iterate through each contour.
			double a = contourArea(contours[i], false);  	 // Find the area of contour
			if (a > largestArea) {
				largestArea = (double)a;
				fieldRect = boundingRect(contours[i]);  	 // Find the bounding rectangle for biggest contour
			}
		}

		rectangle(frame, fieldRect,  Scalar(36,153,255), 1, 8, 0); //draw a rectangle of the largest found 
		//printf("xy(%d - %d), width: %d, height: %d\n", fieldRect.x, fieldRect.y, fieldRect.width, fieldRect.height);

		imshow("field", frame);	

		if (fieldRect.width > 0 && fieldRect.height > 0) ++take10Frames; //make sure the table is detected
		else take10Frames = 0;

	}	while (take10Frames != 10 && waitKey(1000 / FRAME_RATE_CALIBRATE) < 0); //wait until we get a stable result 


		//hardcode inner field, because the camera height will be fixed
		Mat croppedField 	 = frame(fieldRect).clone();	//clone the frame but only display the table, based on the ROI (region of interest)

		//imshow( "field", croppedField );

		innerFieldRect.x = fieldRect.x + 10;				//hardcode the innerField dimensions
		innerFieldRect.y = fieldRect.y + 15;				//
		innerFieldRect.width = fieldRect.width - 10;		//
		innerFieldRect.height = fieldRect.height - 30;		//


		rectangle(frame, innerFieldRect,  Scalar(36,153,255), 1, 8, 0); 		//draw a rectangle of the largest found
		rectangle(croppedField, innerFieldRect,  Scalar(36,153,255), 1, 8, 0);  //draw a rectangle of the largest found 
		//imshow("mini field", frame);
	return 0;
}

//load the camera and change the settings to meet our requirements
bool BallDetection::setCamera() {
	if(!cap.isOpened())  // check if we succeeded
		return false;
	cap.set(CV_CAP_PROP_FPS, FRAME_RATE);					//set framerate
	cap.set(CV_CAP_PROP_FRAME_WIDTH,CAM_FRAME_WIDTH);		//set frame dimensions
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,CAM_FRAME_HEIGHT); 	//
	return true;
}

void BallDetection::forceExitOnKeyPress() {
	cout << "force exit " << this << endl;
	while (parametersFlag & FLAG_CAPTURE) {
		if (GetAsyncKeyState(VK_ESCAPE)) {
			requestExitProgram = true;
			return;
		}
		Sleep(30);
	}
}