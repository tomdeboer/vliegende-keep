#include "stdafx.h"

#ifndef USB_CONNECTION
#define USB_CONNECTION

#define BUF_SIZE 		4			//size of bufferblock to send over USB

// Device vendor and product id.
#define MY_VID 			0x04CC
#define MY_PID 			0x1A62

// Device configuration and interface id.
#define MY_CONFIG 		1
#define MY_INTF 		0

// Device endpoint(s)
#define EP_IN 			0x82
#define EP_OUT 			0x01


using namespace std;

class UsbConnection {
private:
	//attributes
	int ret = 0;
	int vendorID, productID;
	usb_dev_handle *dev = NULL;		//handle for USB communication to ISR1362 on Altera DE2
	char tbuf[BUF_SIZE];		//transfer buffer 

public:
	//constructors
	inline UsbConnection() : UsbConnection(MY_VID, MY_PID) { }
	UsbConnection(int vID, int pID);

	//getters-setters
	inline usb_dev_handle* getDev() { return dev; }
	inline const char* getTbuf() { return tbuf; }
	inline void setTbuf(int index, int value) { tbuf[index] = value; }
	inline int bulkWrite() { return usb_bulk_write(dev, EP_OUT, tbuf, sizeof(tbuf), 500); //write the data to the ISP1362
	}
private:
	//other functions
	usb_dev_handle *open_dev(void);	//find the required USB connection

public:
	int testUsbConnection();		//test the USB connection
};

#endif