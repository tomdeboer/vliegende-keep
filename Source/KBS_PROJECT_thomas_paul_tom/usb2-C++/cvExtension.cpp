#define _CRT_SECURE_NO_WARNINGS

//we used a namespace for these general purpose functions, as suggested on the following website:
//http://stackoverflow.com/questions/9321/how-do-you-create-a-static-class-in-c

#include "stdafx.h"

namespace cvExtension {
	using namespace cv;
	//filter the image to only display orange objects (in the range of the ball color)
	void colorFilter(Mat& src, Scalar rangeMin, Scalar rangeMax) {
	    assert(src.type() == CV_8UC3);		//make sure the image is of the correct type
		cvtColor(src, src, COLOR_BGR2HSV); 	//change the colors to HSV (required for algorithm)
	 	cv::inRange(src, rangeMin, rangeMax,src); //bright ball
	}

	//filter the image to only display movement, by comparing the difference between two frames
	void movementFilter(Mat& lastFrame, Mat& currentFrame,
		Mat& differenceImage, Mat& thresholdImage) {

		if (!currentFrame.cols || !lastFrame.cols) return;		//one of the frames has no defined size
		
		cvtColor(lastFrame, lastFrame, COLOR_BGR2GRAY);	//image to grayscale
		cvtColor(currentFrame, currentFrame, COLOR_BGR2GRAY);	//image to grayscale (required for the algorithm)
		absdiff(lastFrame, currentFrame, differenceImage);	//detect movement algorithm
		medianBlur(differenceImage, differenceImage, 7);						//blur the difference image, to remove noise
		threshold(differenceImage, thresholdImage, 5, 255, THRESH_BINARY);		//remove differences based on lighting. create a binary image of the remaining differences
		//imshow("THRESHOLD", thresholdImage);									//show the result

		int largestArea = 0;			// store the largest area
		vector<vector<Point>> contours; // Vector for storing contour
		vector<Vec4i> hierarchy;		// Contour in contour
		Rect objectRect;				// store the bounding rectangle of the largest object
		findContours(thresholdImage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE); // Find the contours in the image

		for (int i = 0; i < (int)contours.size(); ++i) { 		 // iterate through each contour.
			double a = contourArea(contours[i], false);  	 // Find the area of contour
			if (a > largestArea) {							 //a larger object is found
				largestArea = (double)a;
				// if (largestArea > 500) {
					objectRect = boundingRect(contours[i]);  // Find the bounding rectangle for biggest contour
				// }

				// cout << largestArea << endl;
				objectRect = boundingRect(contours[i]);  // Find the bounding rectangle for biggest contour
			}
		}

		rectangle(currentFrame, objectRect, Scalar(36, 153, 255), 1, 8, 0); //draw a rectangle of the largest found

		//imshow("DIFFERENCE", differenceImage);
		//imshow("ORIGINAL", currentFrame);
	}

}