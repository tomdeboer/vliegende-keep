#ifndef BALL_DETECTION
#define BALL_DETECTION

#define _CRT_SECURE_NO_WARNINGS
#define DEBUG

// Device vendor and product id.
#define MY_VID 			0x04CC
#define MY_PID 			0x1A62


#define FRAME_COUNT  	1800
#define FLAG_CAPTURE 	0x1
#define FLAG_SAVE 	 	0x2
#define FLAG_LOAD 	 	0x4
#define TOPVIEW_CAMERA 	2

#define X_OPCODE 		0x40
#define Y_OPCODE 		0x80

#define FRAME_RATE		180
#define FRAME_RATE_CALIBRATE 4
#define CAM_FRAME_WIDTH  320
#define CAM_FRAME_HEIGHT 240
#define RETURN_SUCCESS	 0

//#define ACTUAL_BORDER_TO_FIELD_RATIO 0.0650406504
#define ACTUAL_BORDER_TO_FIELD_RATIO 0.0731707317W

#define BALL_PIX_WIDTH_MAX 9
#define BALL_PIX_HEIGHT_MAX 9
#define MAX_PIX_BALL_SPEED 20

#define BRIGHT_BALL
//#define PRO_BALL

#include "usbConnection.h"

using namespace std;
using namespace cv;

class BallDetection {
private:
	//attributes
	int ret;
	volatile byte parametersFlag;		//flaggroup to decide which actions are to be performed by the application
	Mat frame, lastFrame;		//last captured frame
	Point storeCentroid[FRAME_COUNT];	//cyclic buffer to store centerpoints of the ball
	Mat storeResult[FRAME_COUNT];		//cyclic buffer 
	string fileName;			//filename of the .xml-file, used for playback
	string window_name;			//name of the display window, showing the ball detection
	Rect innerFieldRect, fieldRect;	//Rectangular properties of the field
	Rect ballRect;				//Rectangular properties of the ball
	VideoCapture cap;			//captures the frames from the camera
	UsbConnection usbConnectionHandle;	//connection with the ISP1362

public:

	//constructors
	BallDetection(); //default

	//getters-setters
	inline const byte getParametersFlag() { return parametersFlag; }
	inline void setParametersFlag(byte flag) { parametersFlag |= flag; }
	inline void removeParametersFlag(byte flag) { parametersFlag &= ~flag; }
	inline const Rect getFieldRect() { return fieldRect; }
	inline UsbConnection* getUsbConnectionHandle() { return &usbConnectionHandle; }

	//other functions
	cv::Point getCentroid(cv::Mat img);
	void saveFilm();
	void loadFilm();
	int captureCamData();
	int defineFieldSize();
	bool setCamera();

	//test functions
	void forceExitOnKeyPress();
};

#endif


//--- tijdelijke storage van onderstaande demo code -------
//---------------------------------------------------------
/*	//wait for all the data to loopback to the device
	ret = usb_bulk_read(dev, EP_IN, rbuf, sizeof(rbuf), 500);
	
	// At some other point get the change of Tick Count
	double elapsedTicks = (double)( GetTickCount() - tickAtStart);
	if (ret < 0) {
		printf("error reading:\n%s\n", usb_strerror());
	}
	else
		printf("success: bulk read %d bytes, TX+RX Bandwidth = %.2f KB/s\n", ret, ret * 2 / 1024/ (elapsedTicks / 1000.0));


	for (int i = 0; i < sizeof(rbuf); i++) {
		if ((rbuf[i] & 0xFF) != (tbuf[i] & 0xFF)) {
			printf("Error data corrupt!%d\n",i);
			break;
		}
	}
*/