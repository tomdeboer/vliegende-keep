#define _CRT_SECURE_NO_WARNINGS

#include "stdafx.h"

#include "ballDetection.h"
#include "usbConnection.h"

using namespace std;
using namespace cv;

atomic<bool> requestExitProgram = false;

int _tmain(int argc, _TCHAR* argv[]) {
	BallDetection bd;

	int err;
	if (argc > 1)
		wcout << argv[1] << endl;

	wcout << "called with " << argc-1 << " argument(s)" << endl;
   
	for (int i = 1; i < argc; i++) {
	   if (_tcscmp(argv[i], _T("capture")) == 0)
		   bd.setParametersFlag(bd.getParametersFlag() | FLAG_CAPTURE); 
	   if (_tcscmp(argv[i], _T("save")) == 0)
		   bd.setParametersFlag(bd.getParametersFlag() | FLAG_SAVE);
	   if (_tcscmp(argv[i], _T("load")) == 0)
		   bd.setParametersFlag(bd.getParametersFlag() | FLAG_LOAD);
	}
	
	if (bd.defineFieldSize() != RETURN_SUCCESS) {
		printf("field size could not be detected\n");
		exit(1);
	} else
		printf("field size: [%d,%d] w: %d, h: %d\n",
		bd.getFieldRect().x, bd.getFieldRect().y, bd.getFieldRect().width, bd.getFieldRect().height);

	// setCamera();
	//==================================== TEMP , JUST REMOVE ==========================
	bd.setParametersFlag(bd.getParametersFlag() | FLAG_CAPTURE);
	//==================================================================================


	setbuf(stdout, NULL);
//    int ret;

	
	// --------------------- USB INTERFACE TESTS -----
	if (bd.getUsbConnectionHandle()->testUsbConnection() != RETURN_SUCCESS) {
		cout << "usb connection with the ISR1362 chip could not be established!" << endl;	
	}
	// ----------------------------------------------------


	char rbuf[BUF_SIZE];
	memset(rbuf,0, sizeof(rbuf));

	
	//--------------- CAPTURE CAM DATA ----------------
	if (bd.getParametersFlag() & FLAG_CAPTURE) {
		cout << "bd " << &bd << endl;
		thread thrCapture(&BallDetection::captureCamData, bd);
		thread thrForceExit(&BallDetection::forceExitOnKeyPress, bd);
		//err = bd.captureCamData();
		//if (err) return -1;
		thrForceExit.join();
		bd.removeParametersFlag(FLAG_CAPTURE);
		thrCapture.join();

		//thrForceExit.detach();

		cout << "thread joined" << endl;
	}

	//--------------- SAVE FILM -----------------------
	if (bd.getParametersFlag() & FLAG_SAVE) {  
		bd.saveFilm();    
	}

	//--------------- LOAD FILM -----------------------
	if (bd.getParametersFlag() & FLAG_LOAD) {
		bd.loadFilm();
	}
	
	usb_release_interface(bd.getUsbConnectionHandle()->getDev(), 0);
	if (bd.getUsbConnectionHandle()->getDev())
		usb_close(bd.getUsbConnectionHandle()->getDev());


	std::cout << "Press ENTER to continue...";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return 0;
}