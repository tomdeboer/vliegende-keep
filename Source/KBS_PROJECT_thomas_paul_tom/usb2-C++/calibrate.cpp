#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main( int argc, char** argv )
{

	VideoCapture cap(2); // open the default camera
	// VideoCapture cap(0); // open the default camera
	if(!cap.isOpened())  // check if we succeeded
		return 1;
	cap.set(CV_CAP_PROP_FPS, 10);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);

// Read original image 
	Mat src; 
	for (int i = 0; i < 255; ++i) {
		cap >> src;
		if (waitKey(300) >= 0) break;
	}
	
// Mat src = imread("Capture.PNG");
		medianBlur(src, src, 7);
		cvtColor(src, src, COLOR_BGR2HSV);
//if fail to read the image
if (!src.data) 
{ 
cout << "Error loading the image" << endl;
return -1; 
}

// Create a window
namedWindow("My Window", WINDOW_NORMAL);
resizeWindow("My Window", 800, 600);

//Create trackbar to change brightness
int iSliderValue1 = 127;
createTrackbar("Scalar H min", "My Window", &iSliderValue1, 255);

//Create trackbar to change contrast
int iSliderValue2 = 127;
createTrackbar("Scalar H max", "My Window", &iSliderValue2, 255);

//Create trackbar to change brightness
int iSliderValue3 = 127;
createTrackbar("Scalar S min", "My Window", &iSliderValue3, 255);

//Create trackbar to change contrast
int iSliderValue4 = 127;
createTrackbar("Scalar S max", "My Window", &iSliderValue4, 255);

//Create trackbar to change brightness
int iSliderValue5 = 127;
createTrackbar("Scalar V min", "My Window", &iSliderValue5, 255);

//Create trackbar to change contrast
int iSliderValue6 = 127;
createTrackbar("Scalar V max", "My Window", &iSliderValue6, 255);


while (true)
{
//Change the brightness and contrast of the image (For more infomation http://opencv-srf.blogspot.com/2013/07/change-contrast-of-image-or-video.html)
Mat dst;
// int iBrightness  = iSliderValue1;
// double dContrast = iSliderValue2 / 50.0;
// src.convertTo(dst, -1, dContrast, iBrightness); 
cv::inRange(src, cv::Scalar(iSliderValue1, iSliderValue3, iSliderValue5), cv::Scalar(iSliderValue2, iSliderValue4, iSliderValue6), dst); //pro-play ball

//show the brightness and contrast adjusted image
imshow("My Window", dst);

// Wait until user press some key for 50ms
int iKey = waitKey(50);

//if user press 'ESC' key
if (iKey == 27)
{
break;
}
}

return 0;
}