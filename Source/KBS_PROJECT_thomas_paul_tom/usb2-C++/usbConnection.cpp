#include "stdafx.h"
#include "usbConnection.h"

using namespace std; //standard C++

UsbConnection::UsbConnection(int vID, int pID) : vendorID(vID), productID(pID) {
	usb_init();             /* initialize the library */
	usb_find_busses();      /* find all busses */
	usb_find_devices();     /* find all connected devices */

	for (int i = 0; i < BUF_SIZE; ++i)
		tbuf[i] = 0;
	dev = open_dev();
}


int UsbConnection::testUsbConnection() {
	if (!(dev = open_dev())) {
		printf("error opening device: \n%s\n", usb_strerror());
		return 1;
	}
	else
		printf("success: device %04X:%04X opened\n", vendorID, productID);
	
	if (usb_set_configuration(dev, MY_CONFIG) < 0) {
		printf("error setting config #%d: %s\n", MY_CONFIG, usb_strerror());
		usb_close(dev);
		return 2;
	}
	else
		printf("success: set configuration #%d\n", MY_CONFIG);
	
	if (usb_claim_interface(dev, 0) < 0) {
		printf("error claiming interface #%d:\n%s\n", MY_INTF, usb_strerror());
		usb_close(dev);
		return 3;
	}
	else
		printf("success: claim_interface #%d\n", MY_INTF);

	return 0; //succes
}

//evaluate all USB connections and find the one with the IDs that correspond with the ISP1362
usb_dev_handle* UsbConnection::open_dev(void) {
	struct usb_bus *bus;
	struct usb_device *dev;

	for (bus = usb_get_busses(); bus; bus = bus->next) {	//loop through all usb busses
		for (dev = bus->devices; dev; dev = dev->next) {	//loop through all devices on those busses
			if (dev->descriptor.idVendor == vendorID && dev->descriptor.idProduct == productID) { //detect the ISP1362
				return usb_open(dev);						//return the pointer to the ISP1362
			}
		}
	}
	return NULL;
}