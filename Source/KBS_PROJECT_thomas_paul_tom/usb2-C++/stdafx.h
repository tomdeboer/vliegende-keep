// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <algorithm>
#include <atomic>
#include <fstream>
#include <iostream>
#include <iterator>
#include <lusb0_usb.h>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/nonfree/nonfree.hpp>
#include <process.h>         // needed for _beginthread()
#include <sstream>
#include <stdio.h>
#include <stdint.h>
#include <string>
#include <tchar.h>
#include <thread>
#include <time.h>
#include <vector>
#include <Windows.h>

// TODO: reference additional headers your program requires here
