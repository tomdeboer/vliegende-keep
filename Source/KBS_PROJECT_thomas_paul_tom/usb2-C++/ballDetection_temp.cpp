#define _CRT_SECURE_NO_WARNINGS
#define DEBUG

// Device vendor and product id.
#define MY_VID 			0x04CC
#define MY_PID 			0x1A62

// Device configuration and interface id.
#define MY_CONFIG 		1
#define MY_INTF 		0

// Device endpoint(s)
#define EP_IN 			0x82
#define EP_OUT 			0x01

#define FRAME_COUNT  	100
#define FLAG_CAPTURE 	0x1
#define FLAG_SAVE 	 	0x2
#define FLAG_LOAD 	 	0x4
#define TOPVIEW_CAMERA 	2

#define X_OPCODE 		0x40
#define Y_OPCODE 		0x80

#define FRAME_RATE		80
#define FRAME_RATE_CALIBRATE 4
#define CAM_FRAME_WIDTH 320
#define CAM_FRAME_HEIGHT 240

#include "stdafx.h"
#include "ballDetection.h"

using namespace std;
using namespace cv;

usb_dev_handle *dev = NULL; /* the device handle */
Mat frame;
Mat storeResult[FRAME_COUNT];
Point storeCentroid[FRAME_COUNT];

vector<Mat> frames;
string window_name = "PS3 eye - Camera";
string fileName = "temp.xml";
byte parameters_flag = 0x00;
Rect fieldRect;				// Rectangular properties of the field

template <typename T>
string numberToString(T pNumber) {
	ostringstream oOStrStream;
	oOStrStream << pNumber;
	return oOStrStream.str();
}

//////////////////////////////////////////////////////////////////////////////
usb_dev_handle *open_dev(void);


#define BUF_SIZE (4)

char tbuf[BUF_SIZE] = {0};
volatile char usb_thread_done = 0;

int _tmain(int argc, _TCHAR* argv[]) {
	int err;
	if (argc > 1)
		wcout << argv[1] << endl;

	wcout << "called with " << argc-1 << " argument(s)" << endl;
   
	for (int i = 1; i < argc; i++) {
	   if (_tcscmp(argv[i], _T("capture")) == 0)
		   parameters_flag |= FLAG_CAPTURE; 
	   if (_tcscmp(argv[i], _T("save")) == 0)
		   parameters_flag |= FLAG_SAVE;
	   if (_tcscmp(argv[i], _T("load")) == 0)
		   parameters_flag |= FLAG_LOAD;
	}
	
	//defineFieldSize();
	//==================================== TEMP , JUST REMOVE ==========================
	parameters_flag |= FLAG_CAPTURE;
	//==================================================================================
	usb_dev_handle *dev = NULL;
	setbuf(stdout, NULL);
//    int ret;
	usb_init();             /* initialize the library */
	usb_find_busses();      /* find all busses */
	usb_find_devices();     /* find all connected devices */
	
	// --------------------- USB INTERFACE TESTS -----
	if (!(dev = open_dev())) {
		printf("error opening device: \n%s\n", usb_strerror());
		return 0;
	}
	else
		printf("success: device %04X:%04X opened\n", MY_VID, MY_PID);	
	
	if (usb_set_configuration(dev, MY_CONFIG) < 0) {
		printf("error setting config #%d: %s\n", MY_CONFIG, usb_strerror());
		usb_close(dev);
		return 0;
	}
	else
		printf("success: set configuration #%d\n", MY_CONFIG);
	
	if (usb_claim_interface(dev, 0) < 0) {
		printf("error claiming interface #%d:\n%s\n", MY_INTF, usb_strerror());
		usb_close(dev);
		return 0;
	}
	else
		printf("success: claim_interface #%d\n", MY_INTF);
	// ----------------------------------------------------


	char rbuf[BUF_SIZE];
	memset(rbuf,0, sizeof(rbuf));


// At some point, get the Tick Count
	DWORD tickAtStart = GetTickCount();

	//start downloading the data
	//_beginthread( usb_thread, 0, (void*)dev );	

	
	//--------------- CAPTURE CAM DATA ----------------
	if (parameters_flag & FLAG_CAPTURE) {
		err = captureCamData();
		if (err) return -1;
	}

	//--------------- SAVE FILM -----------------------
	if (parameters_flag & FLAG_SAVE) {  
		saveFilm();    
	}

	//--------------- LOAD FILM -----------------------
	if (parameters_flag & FLAG_LOAD) {
		loadFilm();
	}
	
	usb_thread_done = 1;
	while(!usb_thread_done);
	usb_release_interface(dev, 0);
	if (dev)
		usb_close(dev);	 


	std::cout << "Press ENTER to continue...";
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	return 0;
}



usb_dev_handle *open_dev(void) {
	struct usb_bus *bus;
	struct usb_device *dev;

	for (bus = usb_get_busses(); bus; bus = bus->next) {
		for (dev = bus->devices; dev; dev = dev->next) {
			if (dev->descriptor.idVendor == MY_VID && dev->descriptor.idProduct == MY_PID) {
				return usb_open(dev);
			}
		}
	}
	return NULL;
}

cv::Point getCentroid(cv::Mat img) {
	cv::Point Coord;
	cv::Moments mm = cv::moments(img,false);
	double moment10 = mm.m10;
	double moment01 = mm.m01;
	double moment00 = mm.m00;
	Coord.x = int(moment10 / moment00);
	Coord.y = int(moment01 / moment00);
	return Coord;
}

void orangeFilter(Mat& src) {
    assert(src.type() == CV_8UC3);
    		//cv::inRange(frame,cv::Scalar(0, 0, 0),cv::Scalar(15,110,110),frame);

			//cv::inRange(frame,cv::Scalar(5, 110, 110),cv::Scalar(15,255,255),frame); //bright orange 18gr ball
	cv::inRange(frame, cv::Scalar(0, 75, 75), cv::Scalar(255, 134, 255), frame); //pro-play ball
    // inRange(src, Scalar(0, 0, 0), Scalar(0, 0, 255), src);
}


//save all the meta-deta from the cards that are stored as pictures in the folder "cards"
void saveFilm() {
	cout << "save Film" << endl;
	FileStorage fs(fileName, FileStorage::WRITE);                                    //use an openCV function to store the data in an 
																					 //XML file
	fs << "frameCount" << FRAME_COUNT;                                               //store how many cards are going to be stored
	time_t rawtime; time(&rawtime);
	fs << "calibrationDate" << asctime(localtime(&rawtime));                         //and at what time it was stored
	for (int i = 0; i < FRAME_COUNT; i++) {                                          //write all data belonging to a card
		//-- Step 3: Store the results in the XML File                               //Store the results to an XML file
		fs << string("frame") + numberToString(i) << "{";
		fs << "Mat" << storeResult[i];
		fs << "centroid" << "{";
			fs << "x" << storeCentroid[i].x;
			fs << "y" << storeCentroid[i].y;
		fs <<   "}";
		fs << "}"; 
	}
	fs.release();                                                                    //close file
}

void loadFilm() {
	cout << "load Film" << endl;
	FileStorage fs(fileName, FileStorage::READ);
	FileNode fn;
	int count = fs["frameCount"];
	//cout << "I found " << count << " frames in this video" << endl;
	for (int i = 0; i < count;++i) {
		string node = string("frame") + numberToString(i);
		// cout << (string)(fs[node]["name"]) << endl;

		fn = fs[node]["Mat"];
		Mat& tempFrame = storeResult[i]; 
		read(fn, tempFrame);

		storeCentroid[i].x = fs[node]["centroid"]["x"];
		storeCentroid[i].y = fs[node]["centroid"]["y"];

	   cout << i + 1 << "    " << storeCentroid[i].x << "  " << storeCentroid[i].y << endl;
	}
	fs.release();


	//---------- SHOW ------
	for(int i = 0; i < FRAME_COUNT; i++) {
		 //line(window_name, scene_corners[0] + Point2f( card.cols, 0), scene_corners[1] + Point2f( card.cols, 0), Scalar(0, 255, 0), 4 );

		Point p = Point(storeCentroid[i]);
		Point q = Point(p);

		p.x -= 5;
		p.y -= 5;
		q.x += 5;
		q.y += 5;

		if (p.x > 0 && p.y > 0) {
			rectangle(storeResult[i], p, q, Scalar(0, 0, 255), 1, 8, 0);
		}

		imshow(window_name, storeResult[++i]);
		// if(waitKey(30) >= 0) break;
		if(waitKey(300) >= 0) break;
	}
	//--------- END SHOW -----------
}

int captureCamData() {
	int ret; 
	cout << "capture cam data" << endl;
	VideoCapture cap(TOPVIEW_CAMERA); // open the default camera
	// VideoCapture cap(0); // open the default camera
	if(!cap.isOpened())  // check if we succeeded
		return 1;
	cap.set(CV_CAP_PROP_FPS, FRAME_RATE);
	cap.set(CV_CAP_PROP_FRAME_WIDTH,CAM_FRAME_WIDTH);
	cap.set(CV_CAP_PROP_FRAME_HEIGHT,CAM_FRAME_HEIGHT);
	//cv::SetCaptureProperty(cap, cv.CV_CAP_PROP_FPS,30);
	//namedWindow(window_name,1);
	  time_t rawtime; 
	  time(&rawtime);
	  cout << asctime(localtime(&rawtime));
	
	// for(int i = 0; i < FRAME_COUNT; i++) { 
	for(;;) { 
		cap >> frame; // get a new frame from camera
		cvtColor(frame, frame, COLOR_BGR2HSV);
		orangeFilter(frame);
		medianBlur(frame, frame, 7);
		cv::Point p = getCentroid(frame);
	   /* storeCentroid[i] = cv::Point(p);*/
		

		if (p.x > 255) {
			tbuf[0] = p.x % 256;
			tbuf[1] = p.x >> 8 | X_OPCODE;
		} else {
			tbuf[0] = p.x;
			tbuf[1] = X_OPCODE;
		}

		if (p.y > 255) {
			tbuf[2] = p.y % 256;
			tbuf[3] = p.y >> 8 | Y_OPCODE;
		} else {
			tbuf[2] = p.y;
			tbuf[3] = Y_OPCODE;
		}

		ret = usb_bulk_write(dev, EP_OUT, tbuf, sizeof(tbuf), 500);

#ifdef DEBUG
		//if (ret < 0)
		//	printf("error writing:\n%s\n", usb_strerror());
		//else {
		//	printf("Success! :Wrote %d bytes\n", ret);
		//}

		Point q = Point(p);

		q.x += 1;
		q.y += 1;

	  //  printf("[%d, %d]", p.x + 5, p.y + 5);
		if (p.x > 0 && p.y > 0) {
		//     //void rectangle(Mat& img, Point pt1, Point pt2, const Scalar& color, int thickness=1, int lineType=8, int shift=0)
			rectangle(frame, p, q, Scalar(0, 0, 0), 1, 8, 0);
		}
#endif        
		imshow(window_name, frame);

		//storeResult[i] = frame;
		
		//cout << i << ". " << "rows: " << frame.rows << endl;
		
	}
	usb_thread_done = 1;
	time(&rawtime);
	cout << asctime(localtime(&rawtime));
	return 0;
}

int defineFieldSize() {
	cout << "define field size" << endl;
	VideoCapture cap(TOPVIEW_CAMERA); // open the default camera
	// VideoCapture cap(0); // open the default camera
	if (!setCamera())
		cout << "Camera not found!\n" << endl;
		exit(2); //camera not found;

	int largestArea;
	int take5Frames = 0;
	//int largestContourIndex;
	vector<vector<Point>> contours; // Vector for storing contour
	vector<Vec4i> hierarchy;		// Contour in contour
	for (;;) {
		cap >> frame; //capture a frame from the camera

		largestArea = 0;
		//largestContourIndex = 0;
		Mat thr(frame.rows,frame.cols,CV_8UC1);
		Mat dst(frame.rows,frame.cols,CV_8UC1,Scalar::all(0)); //black image of same size
		cvtColor(frame,thr,CV_BGR2GRAY); 					   //Convert to gray
		threshold(thr, thr,80, 100,THRESH_BINARY); 			   //Threshold the gray

 
		findContours( thr, contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE ); // Find the contours in the image
   
		for( int i = 0; i < contours.size(); ++i) { 		 // iterate through each contour.
			double a = contourArea(contours[i], false);  // Find the area of contour
			if (a > largestArea) {
				largestArea = a;
				fieldRect = boundingRect(contours[i]);  // Find the bounding rectangle for biggest contour
			}
      	}

		rectangle(frame, fieldRect,  Scalar(36,153,255), 1, 8, 0); //draw a rectangle of the largest found 
		printf("xy(%d - %d), width: %d, height: %d\n",fieldRect.x, fieldRect.y, fieldRect.width, fieldRect.height);
		imshow( "field", frame );
		if (fieldRect.width > 0 && fieldRect.height > 0) ++take5Frames;
		if (take5Frames == 5 || waitKey(1000 / FRAME_RATE_CALIBRATE) >= 0) break;
	}
	return 0;
}
