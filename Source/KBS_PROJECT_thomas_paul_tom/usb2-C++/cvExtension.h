namespace cvExtension {
	void colorFilter(cv::Mat& src, cv::Scalar rangeMin, cv::Scalar rangeMax);
	void movementFilter(cv::Mat& lastFrame, cv::Mat& currentFrame, cv::Mat& differenceImage, cv::Mat& thresholdImage);
}