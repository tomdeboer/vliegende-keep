/*
 * globals.h
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen & Tom de Boer
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "stdafx.h"
#include "panel.h"

//defines
#define bool            BOOLEAN
#define false           0
#define true            1
#define TASK_STACKSIZE  4096
#define SCOREA          ((score_register >> 0) & 0xFF)
#define SCOREB          ((score_register >> 8) & 0xFF)
#define LINE_LENGTH		16

//Definition of Task Priorities
#define START_PRIORITY            6
#define UPDATE_PANEL_PRIORITY     7
#define TASK2_PRIORITY            8

Panel     currentPanel;

INT8U     possession[2];
INT32U*   currentPanelValue;
char	  lcdMessage[33];

// NIOS registers
#define lcd_register ((LCD_REGISTER*) 0x00800010)
#define lcd_chars    ((char*)         0x00800020)


INT32U avgSpeed;

#define xPos (*(INT32U*)0x00800030)
#define yPos (*(INT32U*)0x00800040)

#define ballSpeed    (*(INT32U*)0x00800070)
#define ballDistance (*(INT32U*)0x00800060)
#define ballSpeedMax (*(INT32U*)0x00800080)

#define panelId (*(INT32U*)0x00800050)

#define score_register (*(INT32U*)0x00800090)





#endif /* GLOBALS_H_ */
