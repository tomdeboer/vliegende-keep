/*
 * tasks.h
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen
 */

#ifndef TASKS_H_
#define TASKS_H_

void TaskStart(void* pdata);
void TaskUpdateCurrentPanel(void* pdata);

#endif /* TASKS_H_ */
