/*
 * LCD_content.h
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen & Tom de Boer
 */

#ifndef LCD_CONTENT_H_
#define LCD_CONTENT_H_

typedef struct {
    char line1[17]; //first line 16 chars + \0
    char line2[17]; //second line 16 chars + \0

    union {   //either get 1 block of 16 bits, or 2 blocks of 8 bits
        struct {
          INT8U valueRed;
          INT8U valueBlue;
        } teamData;

        struct {
          INT32U x;
          INT32U y;
        } ballData;

      INT32U valueOther;
    } value;

} LCD_content;

typedef struct {
	INT16U character_index;
	INT16U flags;
} LCD_REGISTER;


#endif /* LCD_CONTENT_H_ */
