/*
 * tasks.c
 *
 *  Created on: 21 mei 2014
 *      Author: Paul van Vulpen & Tom de Boer
 */

#include "stdafx.h"
#include "panel.h"
#include "LCD_content.h"
#include "enum_panel.h"
#include "tasks.h"
#include "globals.h"

OS_STK    tStartStack[TASK_STACKSIZE];    // stack van TaskStart
OS_STK    tUpdateCurrentPanelStack[TASK_STACKSIZE];

void TaskStart(void* pdata) {
  //--------------------- D O E S N ' T   E X I S T  Y E T ----------
  //LCD_clear();
  //--------------------- D O E S N ' T   E X I S T  Y E T ----------

    OSTaskCreate(TaskUpdateCurrentPanel, (void *) 0,
      &tUpdateCurrentPanelStack[TASK_STACKSIZE - 1], UPDATE_PANEL_PRIORITY);
    printf("activated\n");
    while (1) {
       OSTimeDlyHMSM(1, 0, 0, 0);
    } //infinite loop
}

void TaskUpdateCurrentPanel(void* pdata) {
  LCD_content dataOut;
  Panel lastPanel = NONE;

  while (1) {
    emptyString(dataOut.line1);
    emptyString(dataOut.line2);

    currentPanel = panelId;

    //printf("Panel ID: %d\n", *panelId);

    //check for a panel- or data change.
    if (dataChanged(lastPanel, &dataOut)) {
        lastPanel = currentPanel;

      //link the new data to the struct
      switch (currentPanel) {
        case NONE: break;
        
        case SCORE:
            strcpy(dataOut.line1, "SCORE:");
            dataOut.value.teamData.valueRed = SCOREA;
            dataOut.value.teamData.valueBlue = SCOREB;
            currentPanelValue = NULL;
            break;

        case XY:
            strcpy(dataOut.line1, "BALLPOSITION:");
            dataOut.value.ballData.x = xPos;
            dataOut.value.ballData.y = yPos;
            currentPanelValue = NULL;
        	  break;

        case POSSESSION:
            strcpy(dataOut.line1, "POSSESSION:");
            dataOut.value.teamData.valueRed = 50;
            dataOut.value.teamData.valueBlue = 50;
            currentPanelValue = NULL;
            break;

        case TOTAL_DISTANCE:
            strcpy(dataOut.line1, "TOTAL DISTANCE:");
            dataOut.value.valueOther = (INT32U) ((float)ballDistance / 180.0f * 0.4f * 2.0f);
            currentPanelValue = &ballDistance;
            break;

        case MAX_SPEED:
        	strcpy(dataOut.line1, "MAX SPEED:");
        	dataOut.value.valueOther = ballSpeedMax;
        	currentPanelValue = &ballSpeedMax;
        	break;

        case CURRENT_SPEED:
        {
        	// Take ball speed from register to prevent race conditions
        	INT32U side = ballSpeed;

        	// Convert to float and square root
        	float speed = (float)side;

        	// Compensate for buffer length: 90frames / 180fps
        	speed = speed * (1 / 0.5);

//        	Debug:
//        	printf("Sqrt: %lu\n", lroundf(speed));

        	speed *= 0.4f; // cm per second

        	side = lroundf(speed);

        	strcpy(dataOut.line1, "CURRENT SPEED:");
        	sprintf(dataOut.line2, "%2d.%02d cm/s", (int)speed, ((int)(speed*100))%100 );
//        	printf("BS3: %lu\n", side);

        }break;
      }

       LCD_prepareContent(currentPanel, &dataOut);
       //       updateLCDMessage(&dataOut);

       printf("%s\n%s\n\n", dataOut.line1, dataOut.line2);
       LCD_printf("%16s%16s", dataOut.line1, dataOut.line2);

       lastPanel = currentPanel;
    }

    OSTimeDlyHMSM(0, 0, 0, 200);
  }
}
