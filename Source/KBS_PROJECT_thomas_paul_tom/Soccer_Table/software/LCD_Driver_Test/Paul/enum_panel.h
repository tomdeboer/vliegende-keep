#ifndef ENUM_PANEL_
#define ENUM_PANEL_

typedef enum {
    NONE           = 0,
    SCORE          = 1,
    XY             = 2,
    POSSESSION     = 3,
    TOTAL_DISTANCE = 4,
    MAX_SPEED      = 5,
    CURRENT_SPEED  = 6
} Panel;

#endif
