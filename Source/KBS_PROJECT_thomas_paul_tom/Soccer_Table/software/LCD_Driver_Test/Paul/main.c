
#include "stdafx.h"
#include "panel.h"
#include "enum_panel.h"
#include "tasks.h"
#include "globals.h"

OS_STK    tStartStack[TASK_STACKSIZE];    // stack van TaskStart
OS_STK    tUpdateCurrentPanelStack[TASK_STACKSIZE];

int main(void) {
  OSInit();
  currentPanel      = CURRENT_SPEED;

  OSTaskCreate(TaskStart, (void *) 0,
    &tStartStack[TASK_STACKSIZE - 1], START_PRIORITY);  

  OSStart();
  return 0; //should never be reached
}

