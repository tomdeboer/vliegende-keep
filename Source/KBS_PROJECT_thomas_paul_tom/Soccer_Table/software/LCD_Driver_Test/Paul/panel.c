/*
 * panel.c
 *
 *  Created on: 20 mei 2014
 *      Author: Paul van Vulpen & Tom de Boer
 */
#include "stdafx.h"
#include "panel.h"
#include "LCD_content.h"
#include "enum_panel.h"
#include "globals.h"

#include <stdarg.h>

// Private:
char lcd_buffer[33] = "                              ";


void LCD_prepareContent(Panel currentPanel, LCD_content* dataIn) {
  //center the headline
  centerString(dataIn -> line1);

  //create the second line based on the values.

  switch (currentPanel) {
      case NONE: break;
	    
      case SCORE:
        sprintf(dataIn -> line2, "%02lu -- %02lu",
                    dataIn -> value.teamData.valueRed,
                    dataIn -> value.teamData.valueBlue);
        centerString(dataIn->line2);
        addRedAndBlueTag(dataIn->line2);
        break;

  	  case POSSESSION:
  	    sprintf(dataIn -> line2, "%2u%%- %2u%%",
  	                dataIn -> value.teamData.valueRed,
  	                dataIn -> value.teamData.valueBlue);
  	    centerString(dataIn -> line2);
  	    addRedAndBlueTag(dataIn -> line2);
  	    break;

  	  case XY:
  		  sprintf(dataIn -> line2, "X:%4lu  Y:%4lu", dataIn -> value.ballData.x, dataIn->value.ballData.y);
  		  break;

  	  case CURRENT_SPEED:
//  		  sprintf(dataIn -> line2, "%4d km/h", dataIn->value.valueOther);
  		  break;

  	  case MAX_SPEED:
  		  sprintf(dataIn -> line2, "%4lu cm/s", dataIn->value.valueOther);
  		  break;

  	  case TOTAL_DISTANCE:
  		  sprintf(dataIn -> line2, "%4lu cm", dataIn->value.valueOther);
  		  break;
        
  	  default:
  		  sprintf(dataIn -> line2, "%lu", dataIn -> value.valueOther);
  		  centerString(dataIn -> line2);
  		  break;
  }

}

void centerString(char * str) {
    char line1[LINE_LENGTH + 1]; //line length + \0
    INT8U pos, i;

    //prepare center position
    pos = (LINE_LENGTH - strlen(str)) / 2;

    //backup original line
    strcpy(line1, str);

    //clear original line
    memset(str, ' ', LINE_LENGTH);

    //center Line 1:
    for (i = 0; pos < LINE_LENGTH && line1[i] != '\0'; ++pos, ++i)
      *((str) + pos) = line1[i];
}

void emptyString(char * str) {
    memset(str, ' ', LINE_LENGTH);
}

//add "R>  <B" to the second line to point to what team the score belongs to
void addRedAndBlueTag(char * str) {
    str[0]  = 'R';
    str[1]  = '>';
    str[LINE_LENGTH-2] = '<';
    str[LINE_LENGTH-1] = 'B';
}

bool dataChanged(Panel lastPanel, LCD_content* data) {
  return true;

  if (lastPanel != currentPanel) return true;

    switch (currentPanel) {
    	case NONE:
    		return false;
        case XY:
        	return true;
        	break;
        default:
          if (data -> value.valueOther == *currentPanelValue)
        	  return false;
          break;
    }

  return true;
}

void updateLCDMessage(LCD_content* dataIn) {
	INT8U i;
	for (i = 0; i < LINE_LENGTH; ++i) {
		lcdMessage[i]		 	  = (dataIn -> line1)[i];
		lcdMessage[i+LINE_LENGTH] = (dataIn -> line2)[i];
	}
	lcdMessage[32] = '\0';
	printf("%s\n", lcdMessage);
}

void LCD_write(char* str, int lcd_index, int len) {

	// Create Rising Edge
	lcd_register->flags = 0x0;

	// Set registers
	lcd_register->character_index = lcd_index;
	memcpy(lcd_chars, str, len);

	// Create Rising Edge
	lcd_register->flags |= ~0;

	// Allow it some time to propagate the changes
	OSTimeDlyHMSM(0, 0, 0, 1);

}

void LCD_writebuffer() {
	// Loops from 0 to 32 in steps of 4
	int i;
	for(i = 0; i < sizeof(lcd_buffer) / 4; ++i){
		LCD_write(&lcd_buffer[i*4], i, 4);
	}

	//	*redleds = (int) index;
}

void LCD_printf ( const char * format, ... ){
  va_list args;
  va_start (args, format);
  vsprintf (lcd_buffer,format, args);
  va_end (args);

  LCD_writebuffer();
  //memset(&lcd_buffer[chars_printed], ' ', sizeof(lcd_buffer) - chars_printed);

}
