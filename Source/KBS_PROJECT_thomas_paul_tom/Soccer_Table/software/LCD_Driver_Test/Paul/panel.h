/*
 * panel.h
 *
 *  Created on: 20 mei 2014
 *      Author: Paul van Vulpen & Tom de Boer
 */

#ifndef PANEL_H_
#define PANEL_H_

#include "stdafx.h"
#include "LCD_content.h"
#include "enum_panel.h"

void LCD_prepareContent(Panel currentPanel, LCD_content* dataIn);
void LCD_printf ( const char * format, ... );
void LCD_writebuffer();
void LCD_write(char* str, int lcd_index, int len);

void emptyString(char * str);
void centerString(char * str);
void addRedAndBlueTag(char * str);
BOOLEAN dataChanged(Panel lastPanel, LCD_content* data);
void updateLCDMessage(LCD_content* dataIn);


#endif /* PANEL_H_ */
