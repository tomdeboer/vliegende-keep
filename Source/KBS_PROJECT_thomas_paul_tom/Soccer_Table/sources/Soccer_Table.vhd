library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use work.coordinates_memory.all;

entity Soccer_Table is

-------------------------------------------------------------------------------
--                             Port Declarations                             --
-------------------------------------------------------------------------------
port (
	-- Inputs
	CLOCK_50             : in std_logic;
	CLOCK_27             : in std_logic;
	KEY                  : in std_logic_vector (3 downto 0);
	SW                   : in std_logic_vector (17 downto 0);

	--  Communication
	UART_RXD             : in std_logic;

	-- Bidirectionals
	GPIO_0               : inout std_logic_vector (35 downto 0);
	GPIO_1               : inout std_logic_vector (35 downto 0);

	--  Memory (SRAM)
	SRAM_DQ              : inout std_logic_vector (15 downto 0);
	
	-- Memory (SDRAM)
	DRAM_DQ				 : inout std_logic_vector (15 downto 0);

	-- Outputs
	--  Simple
	LEDG                 : out std_logic_vector (8 downto 0);
	LEDR                 : out std_logic_vector (17 downto 0);
	HEX0                 : out std_logic_vector (6 downto 0);
	HEX1                 : out std_logic_vector (6 downto 0);
	HEX2                 : out std_logic_vector (6 downto 0);
	HEX3                 : out std_logic_vector (6 downto 0);
	HEX4                 : out std_logic_vector (6 downto 0);
	HEX5                 : out std_logic_vector (6 downto 0);
	HEX6                 : out std_logic_vector (6 downto 0);
	HEX7                 : out std_logic_vector (6 downto 0);

	--  Memory (SRAM)
	SRAM_ADDR            : out std_logic_vector (17 downto 0);
	SRAM_CE_N            : out std_logic;
	SRAM_WE_N            : out std_logic;
	SRAM_OE_N            : out std_logic;
	SRAM_UB_N            : out std_logic;
	SRAM_LB_N            : out std_logic;

	--  Communication
	UART_TXD             : out std_logic;
	
	-- Memory (SDRAM)
	DRAM_ADDR			 : out std_logic_vector (11 downto 0);
	DRAM_BA_1			 : buffer std_logic;
	DRAM_BA_0			 : buffer std_logic;
	DRAM_CAS_N			 : out std_logic;
	DRAM_RAS_N			 : out std_logic;
	DRAM_CLK			 : out std_logic;
	DRAM_CKE			 : out std_logic;
	DRAM_CS_N			 : out std_logic;
	DRAM_WE_N			 : out std_logic;
	DRAM_UDQM			 : buffer std_logic;
	DRAM_LDQM			 : buffer std_logic;
	GOAL_PIO				:out std_logic_vector(8 downto 0);
	
	-- USB Command
	--DE2 board config 
	OTG_FSPEED	: out		std_logic;						--USB Full Speed 
	OTG_LSPEED	: out		std_logic;						--USB Low Speed 
	--ISP1368	
	OTG_DATA	: inout		std_logic_vector(15 downto 0);	--ISP1362 Data bus 16 bits
	OTG_INT1	: in		std_logic;									--ISP1362 Interrupt 2 (Peripheral Interrupts) 
	OTG_RST_N	: out		std_logic;								--ISP1362 Reset pin
	OTG_ADDR	: out		std_logic_vector(1 downto 0);			--ISP1362 Address 2 Bits[peripheral,command]
	OTG_CS_N	: out		std_logic;									--ISP1362 Chip Select 
	OTG_RD_N	: out		std_logic;									--ISP1362 Write 
	OTG_WR_N	: out		std_logic;									--ISP1362 Read 	
	-- not used
	OTG_DACK0_N	: out		std_logic;								--ISP1362 DMA Acknowledge 1 
	OTG_DACK1_N	: out		std_logic;								--ISP1362 DMA Acknowledge 2
	
	-- LCD_COntroller
	LCD_DATA: OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- 8 Bits data
	LCD_RW,	LCD_EN, LCD_RS: OUT STD_LOGIC;     -- Control signals
	LCD_ON, LCD_BLON: OUT STD_LOGIC;            -- Enable and blacklight (BL doesnt work yet)
	goal_pio_external_interface_export   : in    std_logic_vector(8 downto 0)  := (others => '0'); --        goal_pio_external_interface.export
	lcd_pio_0_external_interface_export  : in std_logic_vector(31 downto 0) := (others => '0');  --       lcd_pio_4_external_interface.export
	lcd_pio_1_external_interface_export  : in std_logic_vector(31 downto 0) := (others => '0'); --       lcd_pio_1_external_interface.export
	lcd_pio_2_external_interface_export  : inout std_logic_vector(31 downto 0) := (others => '0') --       lcd_pio_2_external_interface.export
		);
end Soccer_Table;


architecture Soccer_Table_Rtl of Soccer_Table is

-------------------------------------------------------------------------------
--                           Subentity Declarations                          --
-------------------------------------------------------------------------------
	component nios_system
		port (
		HEX4_from_the_HEX7_HEX4              : out   std_logic_vector(6 downto 0);                     --       HEX7_HEX4_external_interface.HEX4
		HEX5_from_the_HEX7_HEX4              : out   std_logic_vector(6 downto 0);                     --                                   .HEX5
		HEX6_from_the_HEX7_HEX4              : out   std_logic_vector(6 downto 0);                     --                                   .HEX6
		HEX7_from_the_HEX7_HEX4              : out   std_logic_vector(6 downto 0);                     --                                   .HEX7
		SRAM_DQ_to_and_from_the_SRAM         : inout std_logic_vector(15 downto 0) := (others => '0'); --            SRAM_external_interface.DQ
		SRAM_ADDR_from_the_SRAM              : out   std_logic_vector(17 downto 0);                    --                                   .ADDR
		SRAM_LB_N_from_the_SRAM              : out   std_logic;                                        --                                   .LB_N
		SRAM_UB_N_from_the_SRAM              : out   std_logic;                                        --                                   .UB_N
		SRAM_CE_N_from_the_SRAM              : out   std_logic;                                        --                                   .CE_N
		SRAM_OE_N_from_the_SRAM              : out   std_logic;                                        --                                   .OE_N
		SRAM_WE_N_from_the_SRAM              : out   std_logic;                                        --                                   .WE_N
		reset_n                              : in    std_logic                     := '0';             --                   clk_clk_in_reset.reset_n
		GPIO_0_to_and_from_the_Expansion_JP1 : inout std_logic_vector(31 downto 0) := (others => '0'); --   Expansion_JP1_external_interface.export
		UART_RXD_to_the_Serial_port          : in    std_logic                     := '0';             --     Serial_port_external_interface.RXD
		UART_TXD_from_the_Serial_port        : out   std_logic;                                        --                                   .TXD
		LEDR_from_the_Red_LEDs               : out   std_logic_vector(17 downto 0);                    --        Red_LEDs_external_interface.export
		zs_addr_from_the_SDRAM               : out   std_logic_vector(11 downto 0);                    --                         SDRAM_wire.addr
		zs_ba_from_the_SDRAM                 : out   std_logic_vector(1 downto 0);                     --                                   .ba
		zs_cas_n_from_the_SDRAM              : out   std_logic;                                        --                                   .cas_n
		zs_cke_from_the_SDRAM                : out   std_logic;                                        --                                   .cke
		zs_cs_n_from_the_SDRAM               : out   std_logic;                                        --                                   .cs_n
		zs_dq_to_and_from_the_SDRAM          : inout std_logic_vector(15 downto 0) := (others => '0'); --                                   .dq
		zs_dqm_from_the_SDRAM                : out   std_logic_vector(1 downto 0);                     --                                   .dqm
		zs_ras_n_from_the_SDRAM              : out   std_logic;                                        --                                   .ras_n
		zs_we_n_from_the_SDRAM               : out   std_logic;                                        --                                   .we_n
		LEDG_from_the_Green_LEDs             : out   std_logic_vector(8 downto 0);                     --      Green_LEDs_external_interface.export
		GPIO_1_to_and_from_the_Expansion_JP2 : inout std_logic_vector(31 downto 0) := (others => '0'); --   Expansion_JP2_external_interface.export
		clk                                  : in    std_logic                     := '0';             --                         clk_clk_in.clk
		KEY_to_the_Pushbuttons               : in    std_logic_vector(3 downto 0)  := (others => '0'); --     Pushbuttons_external_interface.export
		SW_to_the_Slider_switches            : in    std_logic_vector(17 downto 0) := (others => '0'); -- Slider_switches_external_interface.export
		HEX0_from_the_HEX3_HEX0              : out   std_logic_vector(6 downto 0);                     --       HEX3_HEX0_external_interface.HEX0
		HEX1_from_the_HEX3_HEX0              : out   std_logic_vector(6 downto 0);                     --                                   .HEX1
		HEX2_from_the_HEX3_HEX0              : out   std_logic_vector(6 downto 0);                     --                                   .HEX2
		HEX3_from_the_HEX3_HEX0              : out   std_logic_vector(6 downto 0);                     --                                   .HEX3
		goal_pio_external_interface_export   : in    std_logic_vector(8 downto 0)  := (others => '0'); --        goal_pio_external_interface.export
		lcd_chars_external_interface_export  : out   std_logic_vector(31 downto 0);                    --       lcd_pio_1_external_interface.export
		lcd_flags_external_interface_export  : inout std_logic_vector(31 downto 0) := (others => '0'); --       lcd_pio_0_external_interface.export
		usb_in_x_external_interface_export   : in    std_logic_vector(15 downto 0) := (others => '0'); --       usb_in_x_external_interface.export
		usb_in_y_external_interface_export   : in    std_logic_vector(15 downto 0) := (others => '0'); --       usb_in_y_external_interface.export
		ball_speed_external_interface_export          : in    std_logic_vector(31 downto 0) := (others => '0'); -- ball_speed_external_interface.export
		ball_total_distance_external_interface_export : in    std_logic_vector(31 downto 0) := (others => '0'); -- ball_total_distance_external_interface.export
		panel_id_external_interface_export            : in    std_logic_vector(7 downto 0)  := (others => '0');  --            panel_id_external_interface.export
		ball_speed_max_external_interface_export      : in    std_logic_vector(15 downto 0) := (others => '0'); --      ball_speed_max_external_interface.export
		score_external_interface_export               : in    std_logic_vector(15 downto 0) := (others => '0')  --               score_external_interface.export
);
	end component;
	
	component sdram_pll
		port (
				 signal inclk0 : IN STD_LOGIC;
				 signal c0 : OUT STD_LOGIC;
				 signal c1 : OUT STD_LOGIC
			 );
	end component;

	component usb is
	port(
		--clock
		usb_reset,clk_50, clk_180	: in	std_logic;
		--DE2 board config 
		OTG_FSPEED_SIGNAL	: out		std_logic;						--USB Full Speed 
		OTG_LSPEED_SIGNAL	: out		std_logic;						--USB Low Speed 
		--ISP1368	
		OTG_DATA_SIGNAL	: inout		std_logic_vector(15 downto 0);	--ISP1362 Data bus 16 bits
		OTG_INT1_SIGNAL	: in		std_logic;									--ISP1362 Interrupt 2 (Peripheral Interrupts) 
		OTG_RST_N_SIGNAL	: out		std_logic;								--ISP1362 Reset pin
		OTG_ADDR_SIGNAL	: out		std_logic_vector(1 downto 0);			--ISP1362 Address 2 Bits[peripheral,command]
		OTG_CS_N_SIGNAL	: out		std_logic;									--ISP1362 Chip Select 
		OTG_RD_N_SIGNAL	: out		std_logic;									--ISP1362 Write 
		OTG_WR_N_SIGNAL	: out		std_logic;									--ISP1362 Read 	
		-- not used
		OTG_DACK0_N_SIGNAL	: out		std_logic;								--ISP1362 DMA Acknowledge 1 
		OTG_DACK1_N_SIGNAL	: out		std_logic;								--ISP1362 DMA Acknowledge 2
		--I/O...
		x_out, y_out		: out std_logic_vector(15 downto 0);
		HEX0_SIGNAL,HEX1_SIGNAL,HEX2_SIGNAL,HEX3_SIGNAL,HEX4_SIGNAL,HEX5_SIGNAL,HEX6_SIGNAL,HEX7_SIGNAL	: out	std_logic_vector(6 downto 0)
	);
	end component;	

	
	-----------------
	-- GOAL COUNTER
	-----------------
	component goal_counter is
		port( goal1_sensor, goal2_sensor: in std_logic;
				reset: in std_logic;
				ScoreA, ScoreB: out integer;
				hex_display1, hex_display2, hex_display3, hex_display4: out std_logic_vector(6 downto 0));
	end component;
	
	alias GOALSENSOR1_VCC is GPIO_1(5);
	alias GOALSENSOR1_GND is GPIO_1(7);
	alias GOALSENSOR1_ACT is GPIO_1(3);
	
	
		
	component lcd_controller IS
	PORT (
		CLOCK_50, reset: IN STD_LOGIC; -- Clock signal and (active high) reset

		-- Control signals to LCD
		LCD_DATA: OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- 8 Bits data
		LCD_RW,	LCD_EN, LCD_RS: OUT STD_LOGIC;     -- Control signals
		LCD_ON, LCD_BLON: OUT STD_LOGIC;            -- Enable and blacklight (BL doesnt work yet)

		-- Shared register containing 4 characters 
		TXT  : in std_LOGIC_VECTOR(31 downto 0) := "00100000" & "00100000" & "00100000" & "00100000";
		
		-- Shared register containing flags and location data for characters,
		-- the first 16 bits are flags, the second 16 bits describe the index of the characters.
		reg: in std_logic_vector(31 downto 0) := (others => '0')	
	);	
	end component;	
	
	component capture_buffer is
		port (
			CLK      : in std_logic;
			X_IN     : in std_logic_vector(BIT_WIDTH-1 downto 0);
			Y_IN     : in std_logic_vector(BIT_WIDTH-1 downto 0);
			X_BUFFER : out xy_mem_type;
			Y_BUFFER : out xy_mem_type 
			-------- DEMONSTRATION IN WAVEFORM ---------------------
			--TEMP_OUT : out std_logic_vector(BIT_WIDTH-1 downto 0);
			--TEMP_OUT_2 : out std_logic_vector(BIT_WIDTH-1 downto 0);
			--------------------------------------------------------
		);
	end component; -- capture_buffer
	

	component Snelheidberekening is 
	  port ( 
			CLOCK_50: in std_logic;
			XY_CLOCK: in std_logic;
			reset   : in std_logic;
	  
			x1: in integer;
			x2: in integer;
			y1: in integer;
			y2: in integer;
			
			distance      : out integer := 0;
			total_distance: out integer := 0;
			speed         : out integer := 0;
			max_speed     : out integer := 0;
			
			done_bit    : out std_logic

	--		start_bit   : out std_logic;
	--		fractions     : out integer := 0;

	);
	end component;
	signal done_bit:std_logic; -- Debugging
	signal       distance_from_berekening,
          total_distance_from_berekening,
			          speed_from_berekening,
			      max_speed_from_berekening:integer;

				
	component PanelSwitch is
	generic(
		panelCount : integer := 7
		);
	port (
		     CLK : in std_logic;
		   RESET : in std_logic;
		BTN_PREV : in std_logic;
		BTN_NEXT : in std_logic;
		PANEL_ID : out integer range 0 to panelCount
	);
	end component;
	signal panel_id_from_switch:integer;



	component PreScaler is
	generic(
		        Hz : integer := 1;
		clock_freq : integer := 50_000_000
	);
	port (CLOCK_IN : in std_logic;
		  CLOCK_OUT : out std_logic
	);
	end component;

	
			
	-------------------------------------------------------------------------------
	--                 Internal Wires and Registers Declarations                 --
	-------------------------------------------------------------------------------
	-- Internal Wires
	-- Used to connect the Nios 2 system clock to the non-shifted output of the PLL
	signal system_clock : STD_LOGIC;
	
	signal HARD_RESET, SOFT_RESET : std_logic; -- Will be assigned to "KEY(0)" and "NOT KEY(1)"

	-- Used to concatenate some SDRAM control signals
	signal			 BA : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal			 DQM : STD_LOGIC_VECTOR(1 DOWNTO 0);

	signal NIOS_HEX0                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX1                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX2                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX3                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX4                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX5                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX6                 : std_logic_vector (6 downto 0);
	signal NIOS_HEX7                 : std_logic_vector (6 downto 0);

	-- Used to test the green leds to the goal_pio
	signal			 LEDG_from_the_Green_LEDs_TO_goal_pio, test : STD_LOGIC_VECTOR(8 DOWNTO 0);

	-- Used to test the lcd signals
	signal			LCD_pio_0_from_OS, LCD_chars_from_OS, LCD_flags_from_OS: STD_LOGIC_VECTOR(31 downto 0);

	-- Hex signal lines from the goal counter.
	signal GOAL_HEX0                 : std_logic_vector (6 downto 0);
	signal GOAL_HEX1                 : std_logic_vector (6 downto 0);
	signal GOAL_HEX2                 : std_logic_vector (6 downto 0);
	signal GOAL_HEX3                 : std_logic_vector (6 downto 0);

	-- Hex signal lines from usb connection
	signal USB_HEX0                 : std_logic_vector (6 downto 0);
	signal USB_HEX1                 : std_logic_vector (6 downto 0);
	signal USB_HEX2                 : std_logic_vector (6 downto 0);
	signal USB_HEX3                 : std_logic_vector (6 downto 0);
	signal USB_HEX4                 : std_logic_vector (6 downto 0);
	signal USB_HEX5                 : std_logic_vector (6 downto 0);
	signal USB_HEX6                 : std_logic_vector (6 downto 0);
	signal USB_HEX7                 : std_logic_vector (6 downto 0);

	-- XY signals
	signal y_from_usb_to_xy_buffer, x_from_usb_to_xy_buffer : std_logic_vector(15 downto 0);
	signal y_from_buffer, x_from_buffer: xy_mem_type;
	signal XY_clock_from_USB:std_logic;
	
	signal ScoreA_from_counter, ScoreB_from_counter : integer;
	
	-----------------------
	-- Clock signals
	-----------------------
	signal clock_32400: std_logic;
	signal clock_180  : std_logic;
	signal clock_1    : std_logic;

begin

	-------------------------------------------------------------------------------
	--                            Combinational Logic                            --
	-------------------------------------------------------------------------------

	HARD_RESET <= KEY(0);     -- System reset is active LOW
	SOFT_RESET <= not KEY(1);     -- System reset is active LOW

	
	
	DRAM_BA_1  <= BA(1);
	DRAM_BA_0  <= BA(0);
	DRAM_UDQM  <= DQM(1);
	DRAM_LDQM  <= DQM(0);

	LEDG <= LEDG_from_the_Green_LEDs_TO_goal_pio;
	test <= LEDG_from_the_Green_LEDs_TO_goal_pio;


	HEX0 <= GOAL_HEX0;
	HEX1 <= GOAL_HEX1;
	HEX2 <= GOAL_HEX2;
	HEX3 <= GOAL_HEX3;











	-------------------------------------------------------------------------------
	--                              Internal Modules                             --
	-------------------------------------------------------------------------------

	NiosII : nios_system
		port map(
			-- 1) global signals:
			clk       								=> system_clock,
			reset_n    								=> HARD_RESET, -- reset = '0'
		
			-- the_Slider_switches
			SW_to_the_Slider_switches				=> SW,
			
			-- the_Pushbuttons
			KEY_to_the_Pushbuttons					=> (KEY(3 downto 1) & "1"),

			-- the_Expansion_JP1
			GPIO_0_to_and_from_the_Expansion_JP1(0)	=> GPIO_0(1),
			GPIO_0_to_and_from_the_Expansion_JP1(13 downto 1)	
													=> GPIO_0(15 downto 3),
			GPIO_0_to_and_from_the_Expansion_JP1(14)=> GPIO_0(17),
			GPIO_0_to_and_from_the_Expansion_JP1(31 downto 15)	
													=> GPIO_0(35 downto 19),

			-- the_Expansion_JP2
			GPIO_1_to_and_from_the_Expansion_JP2(0)	=> GPIO_1(1),
			GPIO_1_to_and_from_the_Expansion_JP2(13 downto 1)	
													=> GPIO_1(15 downto 3),
			GPIO_1_to_and_from_the_Expansion_JP2(14)=> GPIO_1(17),
			GPIO_1_to_and_from_the_Expansion_JP2(31 downto 15)	
													=> GPIO_1(35 downto 19),

			-- the_Green_LEDs
			LEDG_from_the_Green_LEDs 				=> LEDG_from_the_Green_LEDs_TO_goal_pio,
			
			-- the_Red_LEDs
			LEDR_from_the_Red_LEDs 					=> LEDR,
			
			-- the_HEX3_HEX0
			HEX0_from_the_HEX3_HEX0 				=> NIOS_HEX0,
			HEX1_from_the_HEX3_HEX0 				=> NIOS_HEX1,
			HEX2_from_the_HEX3_HEX0 				=> NIOS_HEX2,
			HEX3_from_the_HEX3_HEX0 				=> NIOS_HEX3,
			
			-- the_HEX7_HEX4
			HEX4_from_the_HEX7_HEX4 				=> NIOS_HEX4,
			HEX5_from_the_HEX7_HEX4					=> NIOS_HEX5,
			HEX6_from_the_HEX7_HEX4					=> NIOS_HEX6,
			HEX7_from_the_HEX7_HEX4					=> NIOS_HEX7,
			
			-- the_SRAM
			SRAM_ADDR_from_the_SRAM					=> SRAM_ADDR,
			SRAM_CE_N_from_the_SRAM					=> SRAM_CE_N,
			SRAM_DQ_to_and_from_the_SRAM			=> SRAM_DQ,
			SRAM_LB_N_from_the_SRAM					=> SRAM_LB_N,
			SRAM_OE_N_from_the_SRAM					=> SRAM_OE_N,
			SRAM_UB_N_from_the_SRAM 				=> SRAM_UB_N,
			SRAM_WE_N_from_the_SRAM 				=> SRAM_WE_N,
			
			-- the_Serial_port
			UART_RXD_to_the_Serial_port				=> UART_RXD,
			UART_TXD_from_the_Serial_port			=> UART_TXD,
			
			-- the_sdram
			zs_addr_from_the_sdram					=> DRAM_ADDR,
			zs_ba_from_the_sdram					=> BA,
			zs_cas_n_from_the_sdram					=> DRAM_CAS_N,
			zs_cke_from_the_sdram					=> DRAM_CKE,
			zs_cs_n_from_the_sdram					=> DRAM_CS_N,
			zs_dq_to_and_from_the_sdram				=> DRAM_DQ,
			zs_dqm_from_the_sdram					=> DQM,
			zs_ras_n_from_the_sdram					=> DRAM_RAS_N,
			zs_we_n_from_the_sdram					=> DRAM_WE_N,
			
			-- the goal_pio
			-- goal_pio_external_connection_export => GOAL_PIO
			goal_pio_external_interface_export => test,
			
			-- the lcd_pio's
			lcd_chars_external_interface_export => LCD_chars_from_OS,
			lcd_flags_external_interface_export => LCD_flags_from_OS,

			usb_in_x_external_interface_export => x_from_usb_to_xy_buffer,
			usb_in_y_external_interface_export => y_from_usb_to_xy_buffer,

			ball_speed_external_interface_export          => std_logic_vector(to_unsigned(speed_from_berekening         , 32)),
			ball_total_distance_external_interface_export => std_logic_vector(to_unsigned(total_distance_from_berekening, 32)),

			panel_id_external_interface_export            => std_logic_vector(to_unsigned(panel_id_from_switch, 8)),
			ball_speed_max_external_interface_export      => std_logic_vector(to_unsigned(max_speed_from_berekening, 16)),
			score_external_interface_export               => std_logic_vector(to_unsigned(ScoreB_from_counter, 8)) & std_logic_vector(to_unsigned(ScoreA_from_counter, 8))  --               score_external_interface.export

		); 
	
	
	
	
	neg_3ns : sdram_pll
		port map (
			inclk0 => CLOCK_50,
			c0     => DRAM_CLK,
			c1     => system_clock
		);

		
		
		
		
	------------------
	-- USB 
	------------------
	usb_Comm : usb
		port map (
		usb_reset					=> HARD_RESET, -- Active low
		clk_50						=>	CLOCK_50,
		clk_180						=>	clock_180,
		OTG_FSPEED_SIGNAL			=>	OTG_FSPEED,
		OTG_LSPEED_SIGNAL			=>	OTG_LSPEED,
		OTG_DATA_SIGNAL			=>	OTG_DATA,
		OTG_INT1_SIGNAL			=> OTG_INT1,
		OTG_RST_N_SIGNAL			=>	OTG_RST_N,
		OTG_ADDR_SIGNAL			=>	OTG_ADDR,
		OTG_CS_N_SIGNAL			=>	OTG_CS_N,
		OTG_RD_N_SIGNAL			=>	OTG_RD_N,
		OTG_WR_N_SIGNAL			=>	OTG_WR_N,
		OTG_DACK0_N_SIGNAL		=>	OTG_DACK0_N,
		OTG_DACK1_N_SIGNAL		=>	OTG_DACK1_N,
		
		x_out							=>	x_from_usb_to_xy_buffer,
		y_out							=> y_from_usb_to_xy_buffer,	

		HEX0_SIGNAL					=> USB_HEX0,
		HEX1_SIGNAL					=> USB_HEX1,
		HEX2_SIGNAL					=> USB_HEX2,
		HEX3_SIGNAL					=> USB_HEX3,
		HEX4_SIGNAL					=> USB_HEX4,
		HEX5_SIGNAL					=> USB_HEX5,
		HEX6_SIGNAL					=> USB_HEX6,
		HEX7_SIGNAL					=> USB_HEX7
		);
	
	
	------------------
	-- GOAL SENSOR 
	------------------
--	GOALSENSOR1_VCC <= '1';
--	GOALSENSOR1_GND <= '0';
	goal_counters: goal_counter
		port map(
			goal1_sensor			=> SW(0), -- or GOALSENSOR1_ACT
			goal2_sensor			=>	SW(1),
			reset						=> SOFT_RESET,
			ScoreA               => ScoreA_from_counter,
			ScoreB               => ScoreB_from_counter,
			hex_display1			=>	GOAL_HEX0,
			hex_display2			=> GOAL_HEX1,
			hex_display3			=> GOAL_HEX2,
			hex_display4			=> GOAL_HEX3
		);

	lcd_screen: lcd_controller
		port map(
			CLOCK_50 		=> CLOCK_50,
			reset 			=> SOFT_RESET,
			LCD_DATA 		=> LCD_DATA,
			LCD_RW			=> LCD_RW,
			LCD_EN			=> LCD_EN,
			LCD_RS			=>	LCD_RS,
			LCD_ON			=> LCD_ON,
			LCD_BLON			=> LCD_BLON,
			TXT				=> LCD_chars_from_OS,
			reg				=> LCD_flags_from_OS
		);

	x_y_capture_buffer: capture_buffer
			port map(
				CLK			=> clock_180,
				X_IN  		=> x_from_usb_to_xy_buffer,
				Y_IN  		=> y_from_usb_to_xy_buffer,
				X_BUFFER 	=> x_from_buffer,
				Y_BUFFER 	=> y_from_buffer
				-------- DEMONSTRATION IN WAVEFORM ---------------------
				--TEMP_OUT : out std_logic_vector(BIT_WIDTH-1 downto 0);
				--TEMP_OUT_2 : out std_logic_vector(BIT_WIDTH-1 downto 0);
				--------------------------------------------------------
			);
		
	speed_calc: Snelheidberekening
		port map(
			CLOCK_50        => CLOCK_50,
			reset           => SOFT_RESET,
			XY_CLOCK        => clock_180,
	  
			x1              => to_integer(unsigned(x_from_buffer(89))),
			x2              => to_integer(unsigned(x_from_buffer(0))),
			y1              => to_integer(unsigned(y_from_buffer(89))),
			y2              => to_integer(unsigned(y_from_buffer(0))),
			
			distance        => distance_from_berekening,
			total_distance  => total_distance_from_berekening,
			speed           => speed_from_berekening,
			max_speed       => max_speed_from_berekening,
			
			done_bit        => done_bit
		);
		
	panel: PanelSwitch
			port map(
		     CLK => clock_180,
		   RESET => SOFT_RESET,
		BTN_PREV => Key(3),
		BTN_NEXT => Key(2),
		PANEL_ID => panel_id_from_switch
	);
	
	map_clock_180: PreScaler
		generic map(Hz => 180)
		port map(
		 CLOCK_IN => CLOCK_50,
		CLOCK_OUT => clock_180
	);
	map_clock_1: PreScaler
		generic map(Hz => 1)
		port map(
		 CLOCK_IN => CLOCK_50,
		CLOCK_OUT => clock_1
	);


	ClockDivide: process(clock_180)
		variable clockticks: integer range 0 to 128;
	begin
		if rising_edge(clock_180) then
			clockticks := clockticks + 1;
			
			HEX7 <= std_logic_vector(to_unsigned(clockticks, HEX7'LENGTH));
		end if;
	end process;
		
end Soccer_Table_Rtl;


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity PreScaler is
	generic(
		        Hz : integer := 1;
		clock_freq : integer := 50_000_000
	);
	port (CLOCK_IN : in std_logic;
		  CLOCK_OUT : out std_logic
	);
end entity; -- PreScaler

architecture PreScaler_arch of PreScaler is
begin
		PreScale_proc : process(CLOCK_IN)
			constant maxTime     : natural := clock_freq / Hz;
			constant halfTime    : natural := maxTime / 2;
			variable currentTick : natural := 0;
		begin
		if (rising_edge(CLOCK_IN)) then			
			currentTick := currentTick + 1;
			if (currentTick <= halfTime) then
				CLOCK_OUT <= '1';
			elsif (currentTick <= maxTime) then
				CLOCK_OUT <= '0';
			else	-- conventional
				CLOCK_OUT <= '0';
			end if;
			
			if currentTick = maxTime then
				currentTick := 0;
			end if;
		end if;
		end process PreScale_proc;	
end architecture PreScaler_arch;