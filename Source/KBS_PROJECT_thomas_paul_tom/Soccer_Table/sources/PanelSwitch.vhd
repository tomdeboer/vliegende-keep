library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity PanelSwitch is
	generic(
		panelCount : integer := 6
		);
	port (
		     CLK : in std_logic;
		   RESET : in std_logic;
		BTN_PREV : in std_logic;
		BTN_NEXT : in std_logic;
		PANEL_ID : out integer range 0 to panelCount
	);
end entity PanelSwitch; -- PanelSwitch

architecture PanelSwitch_arch of PanelSwitch is
begin
	switch_panel_proc : process(CLK, RESET)
	variable currentPanel : integer range 0 to panelCount := 1;
	variable lastKey : integer range 0 to 2;
	variable buttonReleased : std_logic;
	begin
		if (RESET = '1') then
			lastKey        := 0;
			buttonReleased := '1';
		elsif (rising_edge(CLK)) then
			if (buttonReleased = '1') then
				if (BTN_PREV = '0') and (currentPanel > 1) then
					currentPanel := currentPanel - 1;
					buttonReleased := '0';
					lastKey := 1;
				elsif (BTN_NEXT = '0') and (currentPanel < panelCount) then
					currentPanel := currentPanel + 1;
					buttonReleased := '0';
					lastKey := 2;
				end if;
			else --button held
				if ((lastKey = 1) and (BTN_PREV = '1')) or
					((lastKey = 2) and (BTN_NEXT = '1')) then
					buttonReleased := '1';
					lastKey := 0;
				end if;
			end if;
			
		end if;

		PANEL_ID <= currentPanel;
		
	end process switch_panel_proc;
end architecture PanelSwitch_arch;