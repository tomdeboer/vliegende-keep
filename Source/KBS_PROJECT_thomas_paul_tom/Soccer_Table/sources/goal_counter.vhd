library ieee;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity goal_counter is
	port( goal1_sensor, goal2_sensor: in std_logic;
			reset: in std_logic;
			ScoreA, ScoreB: out integer;
			hex_display1, hex_display2, hex_display3, hex_display4: out std_logic_vector(6 downto 0));
end entity goal_counter;

architecture counterFlow of goal_counter is
	--SevenSegment component
	component sevenSegment is
		port(	C: in std_logic_vector(3 downto 0);
				display : out std_logic_vector(6 downto 0));
	end component;
	
	component Integer_To_Nibbles is
		generic ( Amount_Seven_Segments : integer := 2 );             --default value is 7
		port (
			int_number : in integer; --Can be speed or distance.
			nibble_number : out std_logic_vector((Amount_Seven_Segments-1)*4+3 DOWNTO 0)
		);
	end component;
	
	signal team1NibbleScore, team2NibbleScore: std_logic_vector(7 downto 0); --2 nibbles, 1 for each hex display
	signal team1IntegerScore, team2IntegerScore: integer := 0;
	
begin
		-- Map to output
		scoreA <= team1IntegerScore;
		scoreB <= team2IntegerScore;

		--Goal display team 1
		intToNibblesTeam1: Integer_To_Nibbles port map(team1IntegerScore, team1NibbleScore);
		sevenSegment0: sevenSegment port map(team1NibbleScore(7 downto 4), hex_display2);
		sevenSegment1: sevenSegment port map(team1NibbleScore(3 downto 0), hex_display1);
		
		--Goal display team 1
		intToNibblesTeam2: Integer_To_Nibbles port map(team2IntegerScore, team2NibbleScore);
		sevenSegment3: sevenSegment port map(team2NibbleScore(7 downto 4), hex_display4);
		sevenSegment4: sevenSegment port map(team2NibbleScore(3 downto 0), hex_display3);
		
		goal1_counter: process(reset, goal1_sensor)
			begin
			if(reset = '1') then				
				team1IntegerScore <= 0;
			elsif(rising_edge(goal1_sensor)) then
				team1IntegerScore <= team1IntegerScore + 1;
			end if;			
		end process;
		
		goal2_counter: process(reset, goal2_sensor)
			begin
			if(reset = '1') then				
				team2IntegerScore <= 0;
			elsif(rising_edge(goal2_sensor)) then
				team2IntegerScore <= team2IntegerScore + 1;
			end if;			
		end process;
		
end architecture counterFlow;