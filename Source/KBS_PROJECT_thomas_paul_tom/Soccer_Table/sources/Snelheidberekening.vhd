
library IEEE; 
use IEEE.std_logic_1164.all; 
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use IEEE.math_complex.all;


entity Snelheidberekening is 
  port ( 
		CLOCK_50: in std_logic;
		XY_CLOCK: in std_logic;
		reset   : in std_logic;
  
      x1: in integer := 0;
      x2: in integer := 0;
      y1: in integer := 0;
      y2: in integer := 0;
		
		distance      : out integer := 0;
		total_distance: out integer := 0;
		speed         : out integer := 0;
		max_speed     : out integer := 0;
		
		done_bit    : out std_logic := '0'

--		start_bit   : out std_logic;
--		fractions     : out integer := 0;

); 
end entity;

architecture Snelheidberekening_Arch of Snelheidberekening is 

	-- Aliassing
	alias clk is CLOCK_50;
	
	-- Constants
	constant u_cpu:integer := 100_000_000; -- Simulator is 100Mhz
	
	-- Temporary signals for square root machine
	signal side_vector:STD_LOGIC_VECTOR(63 DOWNTO 0);   -- Side_int as binary for use in square root
	signal distance_vector:STD_LOGIC_VECTOR(31 DOWNTO 0); -- Distance is sqrt(side)
	signal fractions_vector:STD_LOGIC_VECTOR(32 DOWNTO 0);  -- Remaining fraction; after decimal point

	-- Coordination
	signal start,done:std_logic := '0';                    -- Know when to start and when to stop
	
	component SquareRoot3
		-- Generic n is defined from entity above
	PORT(
	                  x: IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
	  clk, reset, start: IN  STD_LOGIC;
	               root: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	          remainder: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	               done: OUT STD_LOGIC
	);
	end component;
begin 

	-- Mapping
	map3: work.SquareRoot3(circuit) port map(
		side_vector,clk,reset,start,distance_vector,fractions_vector,done
	);	

	--start_bit <= start; -- For debugging and visibility
	done_bit  <= done;  -- For debugging and visibility

	-- Logic to start a new square root calculation
	-- as soon as the last one was done.
	process(clk,start,done,XY_CLOCK)
		variable var_total_distance:integer := 0; -- Total distance covered by the ball
		variable var_max_speed     :integer := 0; -- Highest speed ever seen!! (since reset)
		variable tmp_speed         :integer := 0; -- Temp variable to apply calculations on
		
		variable side_int :integer;
	begin
		if  reset = '1' then
			-- TODO
		elsif (rising_edge(clk) and reset = '0') then
		
			-- Reset start bit if it is was set in the previous clock cycle
			if(start = '1') then
				start <= '0';
			end if;
			
			-- Detect change in x1,x2,y1,y2 values.
			-- If change is detected, set start bit
			if XY_CLOCK = '1' then
				
				-- Calculate side
				side_int := ((abs(x2-x1))**2) + ((abs(y2-y1))**2);
				
				-- Convert to binary
				side_vector <= std_logic_vector(to_unsigned(side_int, side_vector'length));

				start <= '1';
			end if;
		end if;

		-- Store calculations
		if  reset = '1' then
			var_total_distance := 0;
			var_max_speed      := 0;
		elsif done'event and done = '1' then

		
			-- Calculate speed
			tmp_speed := to_integer(unsigned(distance_vector));
		
			if tmp_speed < 2 then
				tmp_speed := 0;
			end if;
			
			-- Variables
			var_total_distance := var_total_distance + tmp_speed;

			-- Outputs
			distance  <= tmp_speed;
			--fractions <= to_integer(unsigned(fractions_vector));
			
			
--			tmp_speed := tmp_speed * 18 * 3600;
--			tmp_speed := tmp_speed / 100; 
--			tmp_speed := tmp_speed * 36;          -- Per hour
			
			speed <= tmp_speed;
			
			if tmp_speed >= var_max_speed then
				var_max_speed := tmp_speed;
				max_speed     <= var_max_speed;
			end if;

			
			total_distance <= var_total_distance;
			
		end if;
	end process;
end architecture;

----------------------------------------------------------------------------
-- SquareRoot3.vhd
--
-- section 10.3 Square Rooters. Non-restoring Algorithm (10.3.2)
--
-- x = x2n-1 x2n-2 ... x0: natural (2.n bits)
-- root = qn-1 qn-2 ... q0: natural (n bits)
-- remainder = rn rn-1 ... r0: natural (n bits)
-- x = q^2 + r, r <= 2q if r is positive
--
-- In this case the remainder is correct if it is non-negative. 
-- In fact, the remainder is equal to (r(n-i)�4^i)�2^n
-- where rn-i is the lastnon-negative remainder
--
----------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
ENTITY SquareRoot3 IS
  GENERIC(n: NATURAL:= 32);
PORT(
  x: IN STD_LOGIC_VECTOR(2*n-1 DOWNTO 0);
  clk, reset, start:IN STD_LOGIC;
  root: OUT STD_LOGIC_VECTOR(n-1 DOWNTO 0);
  remainder: OUT STD_LOGIC_VECTOR(n DOWNTO 0);
  done: OUT STD_LOGIC
);
END SquareRoot3;

ARCHITECTURE circuit OF SquareRoot3 IS
  SIGNAL r, next_r: STD_LOGIC_VECTOR(3*n+1 DOWNTO 0);
  SIGNAL q: STD_LOGIC_VECTOR(n-1 DOWNTO 0);
  SIGNAL sumdif: STD_LOGIC_VECTOR(n+1 DOWNTO 0);
  SIGNAL load, update: STD_LOGIC;
  
  SUBTYPE index IS NATURAL RANGE 0 TO n-1;
  SIGNAL count: index;
  TYPE state IS RANGE 0 TO 3;
  SIGNAL current_state: state;
  
  SIGNAL left_operand, right_operand: STD_LOGIC_VECTOR(n+1 DOWNTO 0);
  
BEGIN

  left_operand <= r(3*n-1 DOWNTO 2*n-2);
  right_operand <= q&r(3*n+1)&'1';

  WITH r(3*n+1) SELECT sumdif <= left_operand - right_operand WHEN '0', left_operand + right_operand WHEN OTHERS;
  next_r <= sumdif&r(2*n-3 DOWNTO 0) &"00";

  remainder_register: PROCESS(clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF load = '1' THEN r(2*n-1 DOWNTO 0) <= x; r(3*n+1 DOWNTO 2*n) <= (OTHERS => '0');
      ELSIF update = '1' THEN r <= next_r;
      END IF;  
    END IF;
  END PROCESS;
  remainder <= r(3*n DOWNTO 2*n);

  quotient_register: PROCESS(clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF load = '1' THEN q <= (OTHERS => '0');
      ELSIF update = '1' THEN q <= q(n-2 DOWNTO 0)&NOT(sumdif(n+1));
      END IF;  
    END IF;
  END PROCESS;
  root <= q;

  counter: PROCESS(clk)
  BEGIN
    IF clk'EVENT and clk = '1' THEN
      IF load = '1' THEN count <= 0; 
      ELSIF update = '1' THEN count <= (count+1) MOD n;
      END IF;
    END IF;
  END PROCESS;

  next_state: PROCESS(clk)
  BEGIN
    IF reset = '1' THEN current_state <= 0;
    ELSIF clk'EVENT AND clk = '1' THEN
      CASE current_state IS
        WHEN 0 => IF start = '0' THEN current_state <= 1; END IF;
        WHEN 1 => IF start = '1' THEN current_state <= 2; END IF;
        WHEN 2 => current_state <= 3;
        WHEN 3 => IF count = n-1 THEN current_state <= 0; END IF;
      END CASE;
    END IF;
  END PROCESS;

  output_function: PROCESS(clk, current_state)
  BEGIN
    CASE current_state IS
      WHEN 0 TO 1 => load <= '0'; update <= '0'; done <= '1';
      WHEN 2 => load <= '1'; update <= '0'; done <= '0';
      WHEN 3 => load <= '0'; update <= '1'; done <= '0';
    END CASE;
  END PROCESS; 
END circuit;
