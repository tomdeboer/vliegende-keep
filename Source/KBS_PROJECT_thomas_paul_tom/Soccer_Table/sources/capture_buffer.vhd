library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package coordinates_memory is
	constant BIT_WIDTH : integer := 16;
	constant BUF_SIZE : integer := 90;
	type xy_mem_type is array(BUF_SIZE-1 downto 0) of std_logic_vector(BIT_WIDTH-1 downto 0);
end coordinates_memory;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.coordinates_memory.all;

entity capture_buffer is
	port (
		CLK      : in std_logic;
		X_IN     : in std_logic_vector(BIT_WIDTH-1 downto 0);
		Y_IN     : in std_logic_vector(BIT_WIDTH-1 downto 0);
		X_BUFFER : out xy_mem_type;
		Y_BUFFER : out xy_mem_type--; 
		
		-------- DEMONSTRATION IN WAVEFORM ---------------------
		--TEMP_OUT : out std_logic_vector(BIT_WIDTH-1 downto 0);
		--TEMP_OUT_2 : out std_logic_vector(BIT_WIDTH-1 downto 0);
		--------------------------------------------------------
	);
end entity capture_buffer; -- capture_buffer

architecture capture_buffer_arch of capture_buffer is
	shared variable X_BUFFER_sig, Y_BUFFER_sig : xy_mem_type;
begin
	
	X_BUFFER <= X_BUFFER_sig;
	Y_BUFFER <= Y_BUFFER_sig;
	
	-------- DEMONSTRATION IN WAVEFORM ---------------------
	--TEMP_OUT <= X_BUFFER_sig(BUF_SIZE-1);
	--TEMP_OUT_2 <= X_BUFFER_sig(BUF_SIZE-2);
	--------------------------------------------------------
	
	proc_store_input : process(CLK)

	begin
		if (rising_edge(CLK)) then
			shift_forward : for i in 1 to BUF_SIZE-1 loop
					X_BUFFER_sig(i-1) := X_BUFFER_sig(i);
					Y_BUFFER_sig(i-1) := Y_BUFFER_sig(i);
			end loop ; -- shift_forward	
			
			X_BUFFER_sig(BUF_SIZE-1) := X_IN;
			Y_BUFFER_sig(BUF_SIZE-1) := Y_IN;
		end if;
	end process proc_store_input;
	
end architecture capture_buffer_arch;