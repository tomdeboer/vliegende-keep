LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.numeric_std.all;

ENTITY lcd_controller IS
PORT (
	CLOCK_50, reset: IN STD_LOGIC; -- Clock signal and (active high) reset

	-- Control signals to LCD
	LCD_DATA: OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- 8 Bits data
	LCD_RW,	LCD_EN, LCD_RS: OUT STD_LOGIC;     -- Control signals
	LCD_ON, LCD_BLON: OUT STD_LOGIC;            -- Enable and blacklight (BL doesnt work yet)

	-- Shared register containing 4 characters 
	TXT  : in std_LOGIC_VECTOR(31 downto 0) := "00100000" & "00100000" & "00100000" & "00100000";  -- 4x ascii space
	
	-- Shared register containing flags and location data for characters,
	-- the first 16 bits are flags, the second 16 bits describe the index of the characters.
	reg: inout std_logic_vector(31 downto 0) := (others => '0')
);
end lcd_controller;

ARCHITECTURE FSMD OF lcd_controller IS
	-- State definitions
	TYPE state_type IS (s1,s2,s3,s4,s10,s11,s12,s13,s20,s21,s22,s23,s24);
	SIGNAL state: state_type;
	
	-- Character type definitions
	SUBTYPE ascii IS STD_LOGIC_VECTOR(7 DOWNTO 0);
	TYPE charArray IS array(0 to 31) OF ascii;
	TYPE initArray IS array(6 downto 0) OF ascii;
	
	-- Clock prescaler settings:
	CONSTANT max     : INTEGER := 5_000;
	SIGNAL clock     : STD_LOGIC := '0';
	
	-- Start-bit signals refresh of characters (actual mapping below)
	signal start_bit:std_LOGIC := '0';
	
	-- LCD initialization sequence codes
	-- 0x38 init four times
	-- 0x06 Entry mode set: Increment One; No Shift
	-- 0x0C Display control: Display ON; Cursor OFF; Blink OFF [Previously: 0x0F Display control: Display ON; Cursor ON; Blink ON]                
	-- 0x01 Display clear
	constant initcode: initArray := (x"38",x"38",x"38",x"38",x"06",x"0C",x"01");
	
	-- Part of the flags register
	signal character_index:integer := 0;
	signal flags:std_logic_vector(15 downto 0);
	
	shared variable characters: charArray := (others => (x"20"));

	
BEGIN

	
	-- Integer conversion
	character_index <= to_integer(unsigned(reg(15 downto 0)));
	flags           <= reg(31 downto 16);

	-- Start-bit mapping
	start_bit <= flags(0); -- or not KEY(2);
	
	
	LCD_ON   <= '1'; -- LCD screen on
	LCD_BLON <= '1'; -- (Not implemented on DE2-board)
	
	txt_control: process(start_bit) begin
		if rising_edge(start_bit) then
			characters(character_index * 4 + 0) := TXT(07 downto 00);
			characters(character_index * 4 + 1) := TXT(15 downto 08);
			characters(character_index * 4 + 2) := TXT(24 downto 16);
			characters(character_index * 4 + 3) := TXT(31 downto 24);
		end if;
	end process;
	
	lcd_control: process(clock, reset, start_bit)
		variable count: integer;
	BEGIN
		if(Reset = '1') THEN
			-- Reset character counter
			count      := 0;
			-- Start over
			state      <= s1;
			
		ELSIF rising_edge(clock) THEN
			
		
			case state IS
			-- LCD initialization sequence
			-- The LCD_DATA is written to the LCD at the falling edge of the E line
			-- therefore we need to toggle the E line for each data write
			when s1 =>
				LCD_DATA <= initcode(count);
				LCD_EN <= '1';	-- EN=1;
				LCD_RS <= '0';	-- RS=0; an instruction
				LCD_RW <= '0';	-- R/W'=0; write
				state <= s2;
			when s2 =>
				LCD_EN <= '0';	-- set EN=0;
				count := count + 1;
				if count < 6 THEN
					state <= s1;
				else
					state <= s10;
				end if;

			-- move cursor to first line of display
			when s10 =>
				LCD_DATA <= x"80";	-- x80 is address of 1st position on first line
				LCD_EN <= '1';	-- EN=1;
				LCD_RS <= '0';	-- RS=0; an instruction
				LCD_RW <= '0';	-- R/W'=0; write
				state <= s11;
			when s11 =>
				LCD_EN <= '0';	-- EN=0; toggle EN
				count := 0;
				state <= s12;

			-- write 1st line text
			when s12 =>
				LCD_DATA <= characters(count);
				LCD_EN <= '1';	-- EN=1;
				LCD_RS <= '1';	-- RS=1; data
				LCD_RW <= '0';	-- R/W'=0; write
				state <= s13;
			when s13 =>
				LCD_EN <= '0';	-- EN=0; toggle EN
				count := count + 1;
				if count < 16 THEN
					state <= s12;
				else
					state <= s20;
				end if;

			-- move cursor to second line of display
			when s20 =>
				LCD_DATA <= x"BF";		-- xBF is address of 1st position on second line
				LCD_EN <= '1';	-- EN=1;
				LCD_RS <= '0';	-- RS=0; an instruction
				LCD_RW <= '0';	-- R/W'=0; write
				state <= s21;
			when s21 =>
				LCD_EN <= '0';	-- EN=0; toggle EN
				count := 15;
				state <= s22;

			-- write 2nd line text
			when s22 =>
				LCD_DATA <= characters(count);
				LCD_EN <= '1';	-- EN=1;
				LCD_RS <= '1';	-- RS=1; data
				LCD_RW <= '0';	-- R/W'=0; write
				state <= s23;
			when s23 =>
				LCD_EN <= '0';	-- set EN=0;
				count := count + 1;
				
				-- If char counter is less than all characters, continue writing them LCD
				if count < 32 THEN
					state <= s22;
					
				-- If all chars are written:
				else
--					start_bit <= '0'; -- Unflag start_bit
					state <= s24;
				end if;
			when others =>
				if start_bit = '1' then
					state <= s10;
				else
					state <= s24;
				end if;
			end case;
		end if;
	end process;

	ClockDivide: process
		variable clockticks: INTEGER RANGE 0 TO max := 0;
		CONSTANT half      : INTEGER := max/2;
	BEGIN
		WAIT UNTIL CLOCK_50'EVENT and CLOCK_50 = '1';
		if clockticks < max THEN
			clockticks := clockticks + 1;
		else
			clockticks := 0;
		end if;
		if clockticks < half THEN
			clock <= '0';
		else
			clock <= '1';
		end if;
	end process;

end FSMD;
