
# (C) 2001-2014 Altera Corporation. All rights reserved.
# Your use of Altera Corporation's design tools, logic functions and 
# other software and tools, and its AMPP partner logic functions, and 
# any output files any of the foregoing (including device programming 
# or simulation files), and any associated documentation or information 
# are expressly subject to the terms and conditions of the Altera 
# Program License Subscription Agreement, Altera MegaCore Function 
# License Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by Altera 
# or its authorized distributors. Please refer to the applicable 
# agreement for further details.

# ACDS 13.0sp1 232 win32 2014.05.20.14:46:22

# ----------------------------------------
# Auto-generated simulation script

# ----------------------------------------
# Initialize the variable
if ![info exists SYSTEM_INSTANCE_NAME] { 
  set SYSTEM_INSTANCE_NAME ""
} elseif { ![ string match "" $SYSTEM_INSTANCE_NAME ] } { 
  set SYSTEM_INSTANCE_NAME "/$SYSTEM_INSTANCE_NAME"
} 

if ![info exists TOP_LEVEL_NAME] { 
  set TOP_LEVEL_NAME "nios_system"
} 

if ![info exists QSYS_SIMDIR] { 
  set QSYS_SIMDIR "./../"
} 

if ![info exists QUARTUS_INSTALL_DIR] { 
  set QUARTUS_INSTALL_DIR "C:/altera/13.0sp1/quartus/"
} 


# ----------------------------------------
# Copy ROM/RAM files to simulation directory
alias file_copy {
  echo "\[exec\] file_copy"
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_ociram_default_contents.dat ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_ociram_default_contents.hex ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_ociram_default_contents.mif ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_a.dat ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_a.hex ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_a.mif ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_b.dat ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_b.hex ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_CPU_rf_ram_b.mif ./
  file copy -force $QSYS_SIMDIR/submodules/nios_system_Onchip_memory.hex ./
}

# ----------------------------------------
# Create compilation libraries
proc ensure_lib { lib } { if ![file isdirectory $lib] { vlib $lib } }
ensure_lib          ./libraries/     
ensure_lib          ./libraries/work/
vmap       work     ./libraries/work/
vmap       work_lib ./libraries/work/
if { ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] } {
  ensure_lib                  ./libraries/altera_ver/      
  vmap       altera_ver       ./libraries/altera_ver/      
  ensure_lib                  ./libraries/lpm_ver/         
  vmap       lpm_ver          ./libraries/lpm_ver/         
  ensure_lib                  ./libraries/sgate_ver/       
  vmap       sgate_ver        ./libraries/sgate_ver/       
  ensure_lib                  ./libraries/altera_mf_ver/   
  vmap       altera_mf_ver    ./libraries/altera_mf_ver/   
  ensure_lib                  ./libraries/altera_lnsim_ver/
  vmap       altera_lnsim_ver ./libraries/altera_lnsim_ver/
  ensure_lib                  ./libraries/cycloneii_ver/   
  vmap       cycloneii_ver    ./libraries/cycloneii_ver/   
  ensure_lib                  ./libraries/altera/          
  vmap       altera           ./libraries/altera/          
  ensure_lib                  ./libraries/lpm/             
  vmap       lpm              ./libraries/lpm/             
  ensure_lib                  ./libraries/sgate/           
  vmap       sgate            ./libraries/sgate/           
  ensure_lib                  ./libraries/altera_mf/       
  vmap       altera_mf        ./libraries/altera_mf/       
  ensure_lib                  ./libraries/altera_lnsim/    
  vmap       altera_lnsim     ./libraries/altera_lnsim/    
  ensure_lib                  ./libraries/cycloneii/       
  vmap       cycloneii        ./libraries/cycloneii/       
}
ensure_lib                                                                             ./libraries/irq_mapper/                                                                 
vmap       irq_mapper                                                                  ./libraries/irq_mapper/                                                                 
ensure_lib                                                                             ./libraries/width_adapter/                                                              
vmap       width_adapter                                                               ./libraries/width_adapter/                                                              
ensure_lib                                                                             ./libraries/rsp_xbar_mux_001/                                                           
vmap       rsp_xbar_mux_001                                                            ./libraries/rsp_xbar_mux_001/                                                           
ensure_lib                                                                             ./libraries/rsp_xbar_mux/                                                               
vmap       rsp_xbar_mux                                                                ./libraries/rsp_xbar_mux/                                                               
ensure_lib                                                                             ./libraries/rsp_xbar_demux_001/                                                         
vmap       rsp_xbar_demux_001                                                          ./libraries/rsp_xbar_demux_001/                                                         
ensure_lib                                                                             ./libraries/rsp_xbar_demux/                                                             
vmap       rsp_xbar_demux                                                              ./libraries/rsp_xbar_demux/                                                             
ensure_lib                                                                             ./libraries/cmd_xbar_mux/                                                               
vmap       cmd_xbar_mux                                                                ./libraries/cmd_xbar_mux/                                                               
ensure_lib                                                                             ./libraries/cmd_xbar_demux_001/                                                         
vmap       cmd_xbar_demux_001                                                          ./libraries/cmd_xbar_demux_001/                                                         
ensure_lib                                                                             ./libraries/cmd_xbar_demux/                                                             
vmap       cmd_xbar_demux                                                              ./libraries/cmd_xbar_demux/                                                             
ensure_lib                                                                             ./libraries/rst_controller/                                                             
vmap       rst_controller                                                              ./libraries/rst_controller/                                                             
ensure_lib                                                                             ./libraries/burst_adapter/                                                              
vmap       burst_adapter                                                               ./libraries/burst_adapter/                                                              
ensure_lib                                                                             ./libraries/id_router_004/                                                              
vmap       id_router_004                                                               ./libraries/id_router_004/                                                              
ensure_lib                                                                             ./libraries/id_router_002/                                                              
vmap       id_router_002                                                               ./libraries/id_router_002/                                                              
ensure_lib                                                                             ./libraries/id_router_001/                                                              
vmap       id_router_001                                                               ./libraries/id_router_001/                                                              
ensure_lib                                                                             ./libraries/id_router/                                                                  
vmap       id_router                                                                   ./libraries/id_router/                                                                  
ensure_lib                                                                             ./libraries/addr_router_001/                                                            
vmap       addr_router_001                                                             ./libraries/addr_router_001/                                                            
ensure_lib                                                                             ./libraries/addr_router/                                                                
vmap       addr_router                                                                 ./libraries/addr_router/                                                                
ensure_lib                                                                             ./libraries/SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo/
vmap       SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo ./libraries/SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo/
ensure_lib                                                                             ./libraries/SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo/  
vmap       SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo   ./libraries/SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo/  
ensure_lib                                                                             ./libraries/SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo/              
vmap       SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo               ./libraries/SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo/              
ensure_lib                                                                             ./libraries/SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo/                
vmap       SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo                 ./libraries/SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo/                
ensure_lib                                                                             ./libraries/CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo/   
vmap       CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo    ./libraries/CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo/   
ensure_lib                                                                             ./libraries/CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent/            
vmap       CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent             ./libraries/CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent/            
ensure_lib                                                                             ./libraries/CPU_instruction_master_translator_avalon_universal_master_0_agent/          
vmap       CPU_instruction_master_translator_avalon_universal_master_0_agent           ./libraries/CPU_instruction_master_translator_avalon_universal_master_0_agent/          
ensure_lib                                                                             ./libraries/CPU_jtag_debug_module_translator/                                           
vmap       CPU_jtag_debug_module_translator                                            ./libraries/CPU_jtag_debug_module_translator/                                           
ensure_lib                                                                             ./libraries/CPU_instruction_master_translator/                                          
vmap       CPU_instruction_master_translator                                           ./libraries/CPU_instruction_master_translator/                                          
ensure_lib                                                                             ./libraries/LCD_flags/                                                                  
vmap       LCD_flags                                                                   ./libraries/LCD_flags/                                                                  
ensure_lib                                                                             ./libraries/LCD_chars/                                                                  
vmap       LCD_chars                                                                   ./libraries/LCD_chars/                                                                  
ensure_lib                                                                             ./libraries/Goal_pio/                                                                   
vmap       Goal_pio                                                                    ./libraries/Goal_pio/                                                                   
ensure_lib                                                                             ./libraries/sysid/                                                                      
vmap       sysid                                                                       ./libraries/sysid/                                                                      
ensure_lib                                                                             ./libraries/CPU/                                                                        
vmap       CPU                                                                         ./libraries/CPU/                                                                        
ensure_lib                                                                             ./libraries/SRAM/                                                                       
vmap       SRAM                                                                        ./libraries/SRAM/                                                                       
ensure_lib                                                                             ./libraries/Serial_port/                                                                
vmap       Serial_port                                                                 ./libraries/Serial_port/                                                                
ensure_lib                                                                             ./libraries/Expansion_JP2/                                                              
vmap       Expansion_JP2                                                               ./libraries/Expansion_JP2/                                                              
ensure_lib                                                                             ./libraries/Expansion_JP1/                                                              
vmap       Expansion_JP1                                                               ./libraries/Expansion_JP1/                                                              
ensure_lib                                                                             ./libraries/Pushbuttons/                                                                
vmap       Pushbuttons                                                                 ./libraries/Pushbuttons/                                                                
ensure_lib                                                                             ./libraries/Slider_switches/                                                            
vmap       Slider_switches                                                             ./libraries/Slider_switches/                                                            
ensure_lib                                                                             ./libraries/HEX7_HEX4/                                                                  
vmap       HEX7_HEX4                                                                   ./libraries/HEX7_HEX4/                                                                  
ensure_lib                                                                             ./libraries/HEX3_HEX0/                                                                  
vmap       HEX3_HEX0                                                                   ./libraries/HEX3_HEX0/                                                                  
ensure_lib                                                                             ./libraries/Green_LEDs/                                                                 
vmap       Green_LEDs                                                                  ./libraries/Green_LEDs/                                                                 
ensure_lib                                                                             ./libraries/Red_LEDs/                                                                   
vmap       Red_LEDs                                                                    ./libraries/Red_LEDs/                                                                   
ensure_lib                                                                             ./libraries/SDRAM/                                                                      
vmap       SDRAM                                                                       ./libraries/SDRAM/                                                                      
ensure_lib                                                                             ./libraries/Interval_timer/                                                             
vmap       Interval_timer                                                              ./libraries/Interval_timer/                                                             
ensure_lib                                                                             ./libraries/JTAG_UART/                                                                  
vmap       JTAG_UART                                                                   ./libraries/JTAG_UART/                                                                  
ensure_lib                                                                             ./libraries/Onchip_memory/                                                              
vmap       Onchip_memory                                                               ./libraries/Onchip_memory/                                                              

# ----------------------------------------
# Compile device library files
alias dev_com {
  echo "\[exec\] dev_com"
  if { ![ string match "*ModelSim ALTERA*" [ vsim -version ] ] } {
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.v"              -work altera_ver      
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.v"                       -work lpm_ver         
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.v"                          -work sgate_ver       
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.v"                      -work altera_mf_ver   
    vlog -sv "$QUARTUS_INSTALL_DIR/eda/sim_lib/mentor/altera_lnsim_for_vhdl.sv"  -work altera_lnsim_ver
    vlog     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneii_atoms.v"                -work cycloneii_ver   
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_syn_attributes.vhd"        -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_standard_functions.vhd"    -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/alt_dspbuilder_package.vhd"       -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_europa_support_lib.vhd"    -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives_components.vhd" -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_primitives.vhd"            -work altera          
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220pack.vhd"                      -work lpm             
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/220model.vhd"                     -work lpm             
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate_pack.vhd"                   -work sgate           
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/sgate.vhd"                        -work sgate           
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf_components.vhd"         -work altera_mf       
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_mf.vhd"                    -work altera_mf       
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/altera_lnsim_components.vhd"      -work altera_lnsim    
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneii_atoms.vhd"              -work cycloneii       
    vcom     "$QUARTUS_INSTALL_DIR/eda/sim_lib/cycloneii_components.vhd"         -work cycloneii       
  }
}

# ----------------------------------------
# Compile the design files in correct order
alias com {
  echo "\[exec\] com"
  vcom     "$QSYS_SIMDIR/submodules/nios_system_irq_mapper.vho"                                                                  -work irq_mapper                                                                 
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_width_adapter.sv"                                                       -work width_adapter                                                              
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_address_alignment.sv"                                                   -work width_adapter                                                              
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_burst_uncompressor.sv"                                                  -work width_adapter                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_rsp_xbar_mux_001.vho"                                                            -work rsp_xbar_mux_001                                                           
  vcom     "$QSYS_SIMDIR/submodules/nios_system_rsp_xbar_mux.vho"                                                                -work rsp_xbar_mux                                                               
  vcom     "$QSYS_SIMDIR/submodules/nios_system_rsp_xbar_demux_001.vho"                                                          -work rsp_xbar_demux_001                                                         
  vcom     "$QSYS_SIMDIR/submodules/nios_system_rsp_xbar_demux.vho"                                                              -work rsp_xbar_demux                                                             
  vcom     "$QSYS_SIMDIR/submodules/nios_system_cmd_xbar_mux.vho"                                                                -work cmd_xbar_mux                                                               
  vcom     "$QSYS_SIMDIR/submodules/nios_system_cmd_xbar_demux_001.vho"                                                          -work cmd_xbar_demux_001                                                         
  vcom     "$QSYS_SIMDIR/submodules/nios_system_cmd_xbar_demux.vho"                                                              -work cmd_xbar_demux                                                             
  vlog     "$QSYS_SIMDIR/submodules/mentor/altera_reset_controller.v"                                                            -work rst_controller                                                             
  vlog     "$QSYS_SIMDIR/submodules/mentor/altera_reset_synchronizer.v"                                                          -work rst_controller                                                             
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_burst_adapter.sv"                                                       -work burst_adapter                                                              
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_address_alignment.sv"                                                   -work burst_adapter                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_id_router_004.vho"                                                               -work id_router_004                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_id_router_002.vho"                                                               -work id_router_002                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_id_router_001.vho"                                                               -work id_router_001                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_id_router.vho"                                                                   -work id_router                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_addr_router_001.vho"                                                             -work addr_router_001                                                            
  vcom     "$QSYS_SIMDIR/submodules/nios_system_addr_router.vho"                                                                 -work addr_router                                                                
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo.vho" -work SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo.vho"   -work SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo.vho"               -work SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo.vho"                 -work SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo                
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo.vho"    -work CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo   
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_slave_agent.sv"                                                         -work CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent            
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_burst_uncompressor.sv"                                                  -work CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent            
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_master_agent.sv"                                                        -work CPU_instruction_master_translator_avalon_universal_master_0_agent          
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_slave_translator.sv"                                                    -work CPU_jtag_debug_module_translator                                           
  vlog -sv "$QSYS_SIMDIR/submodules/mentor/altera_merlin_master_translator.sv"                                                   -work CPU_instruction_master_translator                                          
  vcom     "$QSYS_SIMDIR/submodules/nios_system_LCD_flags.vhd"                                                                   -work LCD_flags                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_LCD_chars.vhd"                                                                   -work LCD_chars                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Goal_pio.vhd"                                                                    -work Goal_pio                                                                   
  vcom     "$QSYS_SIMDIR/submodules/nios_system_sysid.vho"                                                                       -work sysid                                                                      
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU.vhd"                                                                         -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_jtag_debug_module_sysclk.vhd"                                                -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_jtag_debug_module_tck.vhd"                                                   -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_jtag_debug_module_wrapper.vhd"                                               -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_oci_test_bench.vhd"                                                          -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_CPU_test_bench.vhd"                                                              -work CPU                                                                        
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SRAM.vhd"                                                                        -work SRAM                                                                       
  vlog     "$QSYS_SIMDIR/submodules/altera_up_rs232_counters.v"                                                                  -work Serial_port                                                                
  vlog     "$QSYS_SIMDIR/submodules/altera_up_rs232_in_deserializer.v"                                                           -work Serial_port                                                                
  vlog     "$QSYS_SIMDIR/submodules/altera_up_rs232_out_serializer.v"                                                            -work Serial_port                                                                
  vlog     "$QSYS_SIMDIR/submodules/altera_up_sync_fifo.v"                                                                       -work Serial_port                                                                
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Serial_port.vhd"                                                                 -work Serial_port                                                                
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Expansion_JP2.vhd"                                                               -work Expansion_JP2                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Expansion_JP1.vhd"                                                               -work Expansion_JP1                                                              
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Pushbuttons.vhd"                                                                 -work Pushbuttons                                                                
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Slider_switches.vhd"                                                             -work Slider_switches                                                            
  vcom     "$QSYS_SIMDIR/submodules/nios_system_HEX7_HEX4.vhd"                                                                   -work HEX7_HEX4                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_HEX3_HEX0.vhd"                                                                   -work HEX3_HEX0                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Green_LEDs.vhd"                                                                  -work Green_LEDs                                                                 
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Red_LEDs.vhd"                                                                    -work Red_LEDs                                                                   
  vcom     "$QSYS_SIMDIR/submodules/nios_system_SDRAM.vhd"                                                                       -work SDRAM                                                                      
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Interval_timer.vhd"                                                              -work Interval_timer                                                             
  vcom     "$QSYS_SIMDIR/submodules/nios_system_JTAG_UART.vhd"                                                                   -work JTAG_UART                                                                  
  vcom     "$QSYS_SIMDIR/submodules/nios_system_Onchip_memory.vhd"                                                               -work Onchip_memory                                                              
  vcom     "$QSYS_SIMDIR/nios_system.vhd"                                                                                                                                                                         
  vcom     "$QSYS_SIMDIR/nios_system_cpu_jtag_debug_module_translator_avalon_universal_slave_0_agent.vhd"                                                                                                         
  vcom     "$QSYS_SIMDIR/nios_system_sdram_s1_translator_avalon_universal_slave_0_agent.vhd"                                                                                                                      
  vcom     "$QSYS_SIMDIR/nios_system_width_adapter.vhd"                                                                                                                                                           
  vcom     "$QSYS_SIMDIR/nios_system_width_adapter_001.vhd"                                                                                                                                                       
  vcom     "$QSYS_SIMDIR/nios_system_cpu_instruction_master_translator.vhd"                                                                                                                                       
  vcom     "$QSYS_SIMDIR/nios_system_cpu_data_master_translator.vhd"                                                                                                                                              
  vcom     "$QSYS_SIMDIR/nios_system_cpu_jtag_debug_module_translator.vhd"                                                                                                                                        
  vcom     "$QSYS_SIMDIR/nios_system_onchip_memory_s1_translator.vhd"                                                                                                                                             
  vcom     "$QSYS_SIMDIR/nios_system_sdram_s1_translator.vhd"                                                                                                                                                     
  vcom     "$QSYS_SIMDIR/nios_system_sram_avalon_sram_slave_translator.vhd"                                                                                                                                       
  vcom     "$QSYS_SIMDIR/nios_system_jtag_uart_avalon_jtag_slave_translator.vhd"                                                                                                                                  
  vcom     "$QSYS_SIMDIR/nios_system_interval_timer_s1_translator.vhd"                                                                                                                                            
  vcom     "$QSYS_SIMDIR/nios_system_sysid_control_slave_translator.vhd"                                                                                                                                          
  vcom     "$QSYS_SIMDIR/nios_system_serial_port_avalon_rs232_slave_translator.vhd"                                                                                                                               
  vcom     "$QSYS_SIMDIR/nios_system_expansion_jp2_avalon_parallel_port_slave_translator.vhd"                                                                                                                     
}

# ----------------------------------------
# Elaborate top level design
alias elab {
  echo "\[exec\] elab"
  vsim -t ps -L work -L work_lib -L irq_mapper -L width_adapter -L rsp_xbar_mux_001 -L rsp_xbar_mux -L rsp_xbar_demux_001 -L rsp_xbar_demux -L cmd_xbar_mux -L cmd_xbar_demux_001 -L cmd_xbar_demux -L rst_controller -L burst_adapter -L id_router_004 -L id_router_002 -L id_router_001 -L id_router -L addr_router_001 -L addr_router -L SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo -L SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo -L SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo -L SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo -L CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo -L CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent -L CPU_instruction_master_translator_avalon_universal_master_0_agent -L CPU_jtag_debug_module_translator -L CPU_instruction_master_translator -L LCD_flags -L LCD_chars -L Goal_pio -L sysid -L CPU -L SRAM -L Serial_port -L Expansion_JP2 -L Expansion_JP1 -L Pushbuttons -L Slider_switches -L HEX7_HEX4 -L HEX3_HEX0 -L Green_LEDs -L Red_LEDs -L SDRAM -L Interval_timer -L JTAG_UART -L Onchip_memory -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneii_ver -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneii $TOP_LEVEL_NAME
}

# ----------------------------------------
# Elaborate the top level design with novopt option
alias elab_debug {
  echo "\[exec\] elab_debug"
  vsim -novopt -t ps -L work -L work_lib -L irq_mapper -L width_adapter -L rsp_xbar_mux_001 -L rsp_xbar_mux -L rsp_xbar_demux_001 -L rsp_xbar_demux -L cmd_xbar_mux -L cmd_xbar_demux_001 -L cmd_xbar_demux -L rst_controller -L burst_adapter -L id_router_004 -L id_router_002 -L id_router_001 -L id_router -L addr_router_001 -L addr_router -L SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rdata_fifo -L SRAM_avalon_sram_slave_translator_avalon_universal_slave_0_agent_rsp_fifo -L SDRAM_s1_translator_avalon_universal_slave_0_agent_rdata_fifo -L SDRAM_s1_translator_avalon_universal_slave_0_agent_rsp_fifo -L CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent_rsp_fifo -L CPU_jtag_debug_module_translator_avalon_universal_slave_0_agent -L CPU_instruction_master_translator_avalon_universal_master_0_agent -L CPU_jtag_debug_module_translator -L CPU_instruction_master_translator -L LCD_flags -L LCD_chars -L Goal_pio -L sysid -L CPU -L SRAM -L Serial_port -L Expansion_JP2 -L Expansion_JP1 -L Pushbuttons -L Slider_switches -L HEX7_HEX4 -L HEX3_HEX0 -L Green_LEDs -L Red_LEDs -L SDRAM -L Interval_timer -L JTAG_UART -L Onchip_memory -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneii_ver -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneii $TOP_LEVEL_NAME
}

# ----------------------------------------
# Compile all the design files and elaborate the top level design
alias ld "
  dev_com
  com
  elab
"

# ----------------------------------------
# Compile all the design files and elaborate the top level design with -novopt
alias ld_debug "
  dev_com
  com
  elab_debug
"

# ----------------------------------------
# Print out user commmand line aliases
alias h {
  echo "List Of Command Line Aliases"
  echo
  echo "file_copy                     -- Copy ROM/RAM files to simulation directory"
  echo
  echo "dev_com                       -- Compile device library files"
  echo
  echo "com                           -- Compile the design files in correct order"
  echo
  echo "elab                          -- Elaborate top level design"
  echo
  echo "elab_debug                    -- Elaborate the top level design with novopt option"
  echo
  echo "ld                            -- Compile all the design files and elaborate the top level design"
  echo
  echo "ld_debug                      -- Compile all the design files and elaborate the top level design with -novopt"
  echo
  echo 
  echo
  echo "List Of Variables"
  echo
  echo "TOP_LEVEL_NAME                -- Top level module name."
  echo
  echo "SYSTEM_INSTANCE_NAME          -- Instantiated system module name inside top level module."
  echo
  echo "QSYS_SIMDIR                   -- Qsys base simulation directory."
  echo
  echo "QUARTUS_INSTALL_DIR           -- Quartus installation directory."
}
file_copy
h
