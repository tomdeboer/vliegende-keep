--IP Functional Simulation Model
--VERSION_BEGIN 13.0 cbx_mgl 2013:06:12:18:04:42:SJ cbx_simgen 2013:06:12:18:03:40:SJ  VERSION_END


-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- You may only use these simulation model output files for simulation
-- purposes and expressly not for synthesis or any other purposes (in which
-- event Altera disclaims all warranties of any kind).


--synopsys translate_off

--synthesis_resources = mux21 15 
 LIBRARY ieee;
 USE ieee.std_logic_1164.all;

 ENTITY  nios_system_addr_router IS 
	 PORT 
	 ( 
		 clk	:	IN  STD_LOGIC;
		 reset	:	IN  STD_LOGIC;
		 sink_data	:	IN  STD_LOGIC_VECTOR (105 DOWNTO 0);
		 sink_endofpacket	:	IN  STD_LOGIC;
		 sink_ready	:	OUT  STD_LOGIC;
		 sink_startofpacket	:	IN  STD_LOGIC;
		 sink_valid	:	IN  STD_LOGIC;
		 src_channel	:	OUT  STD_LOGIC_VECTOR (19 DOWNTO 0);
		 src_data	:	OUT  STD_LOGIC_VECTOR (105 DOWNTO 0);
		 src_endofpacket	:	OUT  STD_LOGIC;
		 src_ready	:	IN  STD_LOGIC;
		 src_startofpacket	:	OUT  STD_LOGIC;
		 src_valid	:	OUT  STD_LOGIC
	 ); 
 END nios_system_addr_router;

 ARCHITECTURE RTL OF nios_system_addr_router IS

	 ATTRIBUTE synthesis_clearbox : natural;
	 ATTRIBUTE synthesis_clearbox OF RTL : ARCHITECTURE IS 1;
	 SIGNAL	wire_nios_system_addr_router_src_channel_71m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_channel_72m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_channel_97m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_channel_98m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_channel_99m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_101m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_102m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_103m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_104m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_105m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_75m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_76m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_77m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_78m_dataout	:	STD_LOGIC;
	 SIGNAL	wire_nios_system_addr_router_src_data_79m_dataout	:	STD_LOGIC;
	 SIGNAL  wire_w1w	:	STD_LOGIC_VECTOR (0 DOWNTO 0);
	 SIGNAL  s_wire_nios_system_addr_router_src_channel_1_430_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_nios_system_addr_router_src_channel_2_459_dataout :	STD_LOGIC;
	 SIGNAL  s_wire_nios_system_addr_router_src_channel_3_488_dataout :	STD_LOGIC;
 BEGIN

	wire_w1w(0) <= NOT s_wire_nios_system_addr_router_src_channel_1_430_dataout;
	s_wire_nios_system_addr_router_src_channel_1_430_dataout <= (((((((((NOT sink_data(55)) AND (NOT sink_data(56))) AND (NOT sink_data(57))) AND (NOT sink_data(58))) AND (NOT sink_data(59))) AND (NOT sink_data(60))) AND (NOT sink_data(61))) AND (NOT sink_data(62))) AND sink_data(63));
	s_wire_nios_system_addr_router_src_channel_2_459_dataout <= (((((((((((((((NOT sink_data(49)) AND (NOT sink_data(50))) AND (NOT sink_data(51))) AND (NOT sink_data(52))) AND (NOT sink_data(53))) AND (NOT sink_data(54))) AND (NOT sink_data(55))) AND (NOT sink_data(56))) AND (NOT sink_data(57))) AND (NOT sink_data(58))) AND (NOT sink_data(59))) AND sink_data(60)) AND (NOT sink_data(61))) AND (NOT sink_data(62))) AND sink_data(63));
	s_wire_nios_system_addr_router_src_channel_3_488_dataout <= (((((((((((((((((NOT sink_data(47)) AND (NOT sink_data(48))) AND (NOT sink_data(49))) AND (NOT sink_data(50))) AND (NOT sink_data(51))) AND (NOT sink_data(52))) AND (NOT sink_data(53))) AND (NOT sink_data(54))) AND (NOT sink_data(55))) AND (NOT sink_data(56))) AND (NOT sink_data(57))) AND (NOT sink_data(58))) AND (NOT sink_data(59))) AND (NOT sink_data(60))) AND sink_data(61)) AND (NOT sink_data(62))) AND sink_data(63));
	sink_ready <= src_ready;
	src_channel <= ( "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & "0" & wire_nios_system_addr_router_src_channel_97m_dataout & wire_nios_system_addr_router_src_channel_98m_dataout & wire_nios_system_addr_router_src_channel_99m_dataout & s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	src_data <= ( sink_data(105 DOWNTO 96) & wire_nios_system_addr_router_src_data_101m_dataout & wire_nios_system_addr_router_src_data_102m_dataout & wire_nios_system_addr_router_src_data_103m_dataout & wire_nios_system_addr_router_src_data_104m_dataout & wire_nios_system_addr_router_src_data_105m_dataout & sink_data(90 DOWNTO 0));
	src_endofpacket <= sink_endofpacket;
	src_startofpacket <= sink_startofpacket;
	src_valid <= sink_valid;
	wire_nios_system_addr_router_src_channel_71m_dataout <= s_wire_nios_system_addr_router_src_channel_1_430_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_2_459_dataout);
	wire_nios_system_addr_router_src_channel_72m_dataout <= wire_w1w(0) AND NOT(s_wire_nios_system_addr_router_src_channel_2_459_dataout);
	wire_nios_system_addr_router_src_channel_97m_dataout <= wire_nios_system_addr_router_src_channel_71m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_channel_98m_dataout <= wire_nios_system_addr_router_src_channel_72m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_channel_99m_dataout <= s_wire_nios_system_addr_router_src_channel_2_459_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_101m_dataout <= wire_nios_system_addr_router_src_data_75m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_102m_dataout <= wire_nios_system_addr_router_src_data_76m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_103m_dataout <= wire_nios_system_addr_router_src_data_77m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_104m_dataout <= wire_nios_system_addr_router_src_data_78m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_105m_dataout <= wire_nios_system_addr_router_src_data_79m_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_3_488_dataout);
	wire_nios_system_addr_router_src_data_75m_dataout <= s_wire_nios_system_addr_router_src_channel_1_430_dataout AND NOT(s_wire_nios_system_addr_router_src_channel_2_459_dataout);
	wire_nios_system_addr_router_src_data_76m_dataout <= wire_w1w(0) OR s_wire_nios_system_addr_router_src_channel_2_459_dataout;
	wire_nios_system_addr_router_src_data_77m_dataout <= wire_w1w(0) AND NOT(s_wire_nios_system_addr_router_src_channel_2_459_dataout);
	wire_nios_system_addr_router_src_data_78m_dataout <= wire_w1w(0) OR s_wire_nios_system_addr_router_src_channel_2_459_dataout;
	wire_nios_system_addr_router_src_data_79m_dataout <= wire_w1w(0) OR s_wire_nios_system_addr_router_src_channel_2_459_dataout;

 END RTL; --nios_system_addr_router
--synopsys translate_on
--VALID FILE
