LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

entity clock_180 is
	port(
		clock_in: in std_logic;
		clock_out: out std_logic
	);	
end entity;

architecture clockFlow of clock_180 is
	CONSTANT maxClockTicks: INTEGER := 27_000;
	CONSTANT prescaler: INTEGER := maxClockTicks / 180; --27.000.000 / 180 = 150.000

begin

	ClockDivide: process(clock_in)
	variable clockticks: integer range 0 to maxClockTicks;
	
	begin
		if(rising_edge(clock_in)) then
			if clockticks < maxClockTicks then
				clockticks := clockticks + 1;
			else
				clockticks := 0;
			end if;

			if clockticks < prescaler then
				clock_out <= '0';
			else
				clock_out <= '1';
			end if;
		end if;
	end process;
	
end clockFlow;