library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Integer_To_Nibbles is
	generic ( Amount_Seven_Segments : integer := 2 );             --default value is 7
	port (
		int_number : in integer; --Can be speed or distance.
		nibble_number : out std_logic_vector((Amount_Seven_Segments-1)*4+3 DOWNTO 0)
	);
end entity ;

architecture Integer_To_Nibbles_number_Flow of Integer_To_Nibbles is

begin

	p1: process(int_number)
		variable remainder: unsigned(3 DOWNTO 0);
		variable quotient: unsigned((Amount_Seven_Segments-1)*4+3 DOWNTO 0);
		
	begin
		quotient := to_unsigned(int_number, quotient'LENGTH);

		for i in 0 to Amount_Seven_Segments-1 loop --Get int_number of decimals for N amount of displays
			remainder 	:= resize(quotient mod 10, 4); -- int_number % 10 == 987 % 10 = 7, 98 % 10 = 8
			quotient 	:= quotient / 10; --remove the last int_number that has been stored as the remainder.
			nibble_number(i*4+3 DOWNTO i*4) <= std_logic_vector(remainder);
		end loop;
		
	end process;
end architecture;