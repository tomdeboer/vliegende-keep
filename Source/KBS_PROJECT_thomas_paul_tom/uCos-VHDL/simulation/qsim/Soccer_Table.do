onerror {quit -f}
vlib work
vlog -work work DE2_Basic_Computer.vo
vlog -work work Soccer_Table.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Soccer_Table_vlg_vec_tst
vcd file -direction Soccer_Table.msim.vcd
vcd add -internal Soccer_Table_vlg_vec_tst/*
vcd add -internal Soccer_Table_vlg_vec_tst/i1/*
add wave /*
run -all
