onerror {quit -f}
vlib work
vlog -work work integer_to_nibbles.vo
vlog -work work integer_to_nibbles.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.Integer_To_Nibbles_vlg_vec_tst
vcd file -direction integer_to_nibbles.msim.vcd
vcd add -internal Integer_To_Nibbles_vlg_vec_tst/*
vcd add -internal Integer_To_Nibbles_vlg_vec_tst/i1/*
add wave /*
run -all
