library verilog;
use verilog.vl_types.all;
entity Integer_To_Nibbles_vlg_check_tst is
    port(
        nibble_number   : in     vl_logic_vector(7 downto 0);
        sampler_rx      : in     vl_logic
    );
end Integer_To_Nibbles_vlg_check_tst;
