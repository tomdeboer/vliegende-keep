library verilog;
use verilog.vl_types.all;
entity Integer_To_Nibbles is
    port(
        int_number      : in     vl_logic_vector(31 downto 0);
        nibble_number   : out    vl_logic_vector(7 downto 0)
    );
end Integer_To_Nibbles;
