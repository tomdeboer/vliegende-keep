library verilog;
use verilog.vl_types.all;
entity Integer_To_Nibbles_vlg_sample_tst is
    port(
        int_number      : in     vl_logic_vector(31 downto 0);
        sampler_tx      : out    vl_logic
    );
end Integer_To_Nibbles_vlg_sample_tst;
