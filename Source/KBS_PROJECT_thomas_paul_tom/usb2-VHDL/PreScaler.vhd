library IEEE;
use IEEE.std_logic_1164.all;

entity PreScaler is
	port (CLOCK_IN : in std_logic;
		  CLOCK_OUT : out std_logic);
end entity PreScaler; -- PreScaler

architecture PreScaler_arch of PreScaler is
begin
		PreScale_proc : process(CLOCK_IN)
		variable Hz : natural := 10_000_000;
		constant maxTime : natural := 50_000_000 / Hz;
		constant halfTime : natural := maxTime / 2;

		variable currentTick : natural := 0;
		begin
		if (rising_edge(CLOCK_IN)) then			
			currentTick := currentTick + 1;
			if (currentTick <= halfTime) then
				CLOCK_OUT <= '1';
			elsif (currentTick <= maxTime) then
				CLOCK_OUT <= '0';
			else	-- conventional
				CLOCK_OUT <= '0';
			end if;
			
			if currentTick = maxTime then
				currentTick := 0;
			end if;
		end if;
		end process PreScale_proc;	
end architecture PreScaler_arch;
	